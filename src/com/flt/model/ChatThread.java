package com.flt.model;

public class ChatThread {
	
	private String chatThreadID;
	private String user1_ID;
	private String user2_id;
	private String user1_name;;
	private String user2_name;
	public String getChatThreadID() {
		return chatThreadID;
	}
	public void setChatThreadID(String chatThreadID) {
		this.chatThreadID = chatThreadID;
	}
	public String getUser1_ID() {
		return user1_ID;
	}
	public void setUser1_ID(String user1_ID) {
		this.user1_ID = user1_ID;
	}
	public String getUser2_id() {
		return user2_id;
	}
	public void setUser2_id(String user2_id) {
		this.user2_id = user2_id;
	}
	public String getUser1_name() {
		return user1_name;
	}
	public void setUser1_name(String user1_name) {
		this.user1_name = user1_name;
	}
	public String getUser2_name() {
		return user2_name;
	}
	public void setUser2_name(String user2_name) {
		this.user2_name = user2_name;
	}
	public ChatThread()
	{
		
	}
	public ChatThread(String chatThreadID, String user1_ID, String user2_id, String user1_name, String user2_name) {
		super();
		this.chatThreadID = chatThreadID;
		this.user1_ID = user1_ID;
		this.user2_id = user2_id;
		this.user1_name = user1_name;
		this.user2_name = user2_name;
	}
	

}
