package com.flt.model;

public class Comment {
	private String comment_id;
	private String news_id;
	private String user_id;
	private String user_name;
	private String date;
	private String message;
	
	public Comment()
	{
		
	}
	public String getComment_id() {
		return comment_id;
	}
	public Comment(String comment_id, String news_id, String user_id, String user_name, String date, String message) {
		super();
		this.comment_id = comment_id;
		this.news_id = news_id;
		this.user_id = user_id;
		this.user_name = user_name;
		this.date = date;
		this.message = message;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public void setComment_id(String comment_id) {
		this.comment_id = comment_id;
	}
	public String getNews_id() {
		return news_id;
	}
	public void setNews_id(String news_id) {
		this.news_id = news_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
