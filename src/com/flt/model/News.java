package com.flt.model;

import java.util.ArrayList;
import java.util.Date;

public class News {
	private String news_object_id;
	private String title;
	private String created_at;
	private String description;
	private String category_id;
	private String category_name;
	private String userObjectId;
	private String profile_name;
	private double price;
	private String university_name;
	private String university_id;
	private Date modified_at;
	private String imageUrl;
	private ArrayList<NewsImage> imageList;
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	
	public String getNews_object_id() {
		return news_object_id;
	}

	public void setNews_object_id(String news_object_id) {
		this.news_object_id = news_object_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public Date getModified_at() {
		return modified_at;
	}

	public void setModified_at(Date modified_at) {
		this.modified_at = modified_at;
	}

	public ArrayList<NewsImage> getImageList() {
		return imageList;
	}

	public void setImageList(ArrayList<NewsImage> imageList) {
		this.imageList = imageList;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getUserObjectId() {
		return userObjectId;
	}

	public void setUserObjectId(String userObjectId) {
		this.userObjectId = userObjectId;
	}

	public String getProfile_name() {
		return profile_name;
	}

	public void setProfile_name(String profile_name) {
		this.profile_name = profile_name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getUniversity_name() {
		return university_name;
	}

	public void setUniversity_name(String university_name) {
		this.university_name = university_name;
	}

	public String getUniversity_id() {
		return university_id;
	}

	public void setUniversity_id(String university_id) {
		this.university_id = university_id;
	}

	


	
	public News()
	{
		
	}
	
	

}
