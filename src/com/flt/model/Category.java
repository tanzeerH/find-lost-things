package com.flt.model;

public class Category {
	
	private String name;
	private String objectId;
	private String ImageUrl;
	public String getName() {
		return name;
	}
	public Category()
	{
		
	}
	public Category(String name, String objectId, String imageUrl) {
		super();
		this.name = name;
		this.objectId = objectId;
		ImageUrl = imageUrl;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getImageUrl() {
		return ImageUrl;
	}
	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}
	
	

}
