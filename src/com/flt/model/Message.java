package com.flt.model;

import java.util.Date;

public class Message {
	
	private String objectId;
	private String message;
	private String threadId;
	private String senderId;
	private String sender_name;
	private Date createdDate;
	public Message()
	{
		
	}
	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getThreadId() {
		return threadId;
	}

	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSender_name() {
		return sender_name;
	}

	public void setSender_name(String sender_name) {
		this.sender_name = sender_name;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Message(String objectId, String message, String threadId, String senderId, String sender_name,
			Date createdDate) {
		super();
		this.objectId = objectId;
		this.message = message;
		this.threadId = threadId;
		this.senderId = senderId;
		this.sender_name = sender_name;
		this.createdDate = createdDate;
	}

	
}
