package com.flt.model;

public class Country {

	private String country_name;
	private String countryObjectId;
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	public String getCountryObjectId() {
		return countryObjectId;
	}
	public void setCountryObjectId(String countryObjectId) {
		this.countryObjectId = countryObjectId;
	}
	public Country(String country_name, String countryObjectId) {
		super();
		this.country_name = country_name;
		this.countryObjectId = countryObjectId;
	}
	public Country()
	{
		
	}
}
