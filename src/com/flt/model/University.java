package com.flt.model;

public class University {

	private String university_name;
	private String univseristy_id;
	private String country_id;
	private String status;
	public University()
	{
		
	}
	public String getUniversity_name() {
		return university_name;
	}
	public void setUniversity_name(String university_name) {
		this.university_name = university_name;
	}
	public String getUnivseristy_id() {
		return univseristy_id;
	}
	public void setUnivseristy_id(String univseristy_id) {
		this.univseristy_id = univseristy_id;
	}
	public String getCountry_id() {
		return country_id;
	}
	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public University(String university_name, String univseristy_id, String country_id, String status) {
		super();
		this.university_name = university_name;
		this.univseristy_id = univseristy_id;
		this.country_id = country_id;
		this.status = status;
	}
}
