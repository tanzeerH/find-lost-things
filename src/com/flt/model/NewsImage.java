package com.flt.model;

public class NewsImage {

	private String objectId;
	private String image_url;
	public NewsImage()
	{
		
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
}
