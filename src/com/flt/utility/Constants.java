package com.flt.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.findlostthings.R;
import com.flt.activity.LoginActivity;
import com.flt.model.News;

public class Constants {
	public static News selectedNews=null;
	public static void alert(Context context,String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		bld.create().show();
	}

}
