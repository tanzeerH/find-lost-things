package com.flt.fltbroadcastreceiver;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.AnalogClock;


import com.findlostthings.R;
import com.flt.activity.AddsActivity;
import com.flt.activity.PostAddActivity;
import com.flt.utility.PreferenceConnector;
import com.parse.Parse;
import com.parse.ParsePushBroadcastReceiver;

public class PushNotificationReceiver extends ParsePushBroadcastReceiver{
	@SuppressLint("NewApi")
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.e("MSG","on receive");
		String intentAction = intent.getAction();
		JSONObject pushData = null;
		if ("com.parse.push.intent.RECEIVE".equals(intentAction)) {
			
			try {
				pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
			} catch (JSONException e) {
				Log.e("com.parse.ParsePushReceiver", "Unexpected JSONException when receiving push data: ");
			}
			
		}
		String threadId=PreferenceConnector.readString(context,PreferenceConnector.KEY_CURRENT_THREAD_ID, "-1");
		Log.e("current thread Id",threadId);
		String receIvedChatId="";
		try {
			receIvedChatId=pushData.getString("chat_id");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("received thread id",receIvedChatId);
		if(threadId.equals(receIvedChatId))
		{
			//send the message to activity
			Intent i=new Intent("com.flt.push");
			i.putExtra("msg",pushData.toString());
			
			Log.e("MSG","here");
			context.sendBroadcast(i);
		}
		else
		{
			//create notification
			Intent i=new Intent(context,PostAddActivity.class);
			PendingIntent pIntent=PendingIntent.getActivity(context, 0, i,0);
			Notification notification=null;
			try {
				notification = new Notification.Builder(context).
						setContentTitle("New message from "+pushData.getString("profile_name")).setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher).setContentText(pushData.getString("message")).setContentIntent(pIntent).build();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
			NotificationManager notificationManager = 
					  (NotificationManager)context.getSystemService("notification");
			notificationManager.notify(0, notification);
			
		}
	}
	
 
}
