package com.flt.activity;

import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.adapter.CommentListAdapter;
import com.flt.adapter.NothingSelectedSpinnerAdapter;
import com.flt.adapter.SimpleSpinnerAdapter;
import com.flt.adapter.UniversitySpinnerAdapter;
import com.flt.model.Country;
import com.flt.model.News;
import com.flt.model.University;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;

public class SignUpActivity extends BaseActivity implements OnClickListener {
	private EditText edtEmail, edtPass, edtConfirmPass, edtProfileName;
	private Button btnSignup;
	private ProgressDialog pdDialog;
	private Spinner spnrCountry, SpnrUni;
	private ArrayList<String> countryList = new ArrayList<String>();
	private ArrayList<University> uniList = new ArrayList<University>();
	private SimpleSpinnerAdapter countryAdapter;
	private UniversitySpinnerAdapter uniAdapter;
	private CheckBox chkUni;
	private boolean flag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		btnSignup = (Button) findViewById(R.id.btn_signup);
		edtEmail = (EditText) findViewById(R.id.et_email);
		edtPass = (EditText) findViewById(R.id.et_pass);
		edtConfirmPass = (EditText) findViewById(R.id.et_cpass);
		edtProfileName = (EditText) findViewById(R.id.et_profile_name);
		chkUni = (CheckBox) findViewById(R.id.chk_uni);

		spnrCountry = (Spinner) findViewById(R.id.sp_country);
		SpnrUni = (Spinner) findViewById(R.id.sp_university);

		pdDialog = new ProgressDialog(SignUpActivity.this);
		pdDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
		btnSignup.setOnClickListener(this);

		setUpInitialUniversities();
		flag = true;
		chkUni.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					if (spnrCountry.getSelectedItemPosition() > 0) {

						showInputDialog();
					} else
						alertCheck(SignUpActivity.this, "Please select your country first.");
				}

			}
		});

		flag = true;
		SpnrUni.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				Log.e("flag", "" + flag);
				if (spnrCountry.getSelectedItemPosition() > 0) {
					return false;
				} else {
					if (flag) {
						flag = false;
						alertCheck(SignUpActivity.this, "Please select your country first.");
					}
					return true;
				}

			}
		});

		if (Utility.hasInternet(SignUpActivity.this)) {
			pdDialog.show();
			setUpCountries();
		} else
			Constants.alert(SignUpActivity.this, "Please check your internet connection.");

		spnrCountry.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				Log.e("position", "" + position);
				if (position > 0) {
					if(Utility.hasInternet(SignUpActivity.this))
					{
						pdDialog.show();
						setUpUniversities(countryList.get(position - 1));
					}
					else
						Constants.alert(SignUpActivity.this,"Please check your internet connection.");
					
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}

		});
	}

	private void setUpInitialUniversities() {
		University university = new University();
		university.setUniversity_name("Placeholder");
		uniList.add(university);
		uniAdapter = new UniversitySpinnerAdapter(SignUpActivity.this, R.layout.row_spinner_submenu, uniList);
		SpnrUni.setAdapter(new NothingSelectedSpinnerAdapter(uniAdapter, R.layout.row_spinner_nothing_selected,
				SignUpActivity.this));
		uniAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	}

	private void setUpCountries() {
		countryList.add("Bangladesh");

		countryAdapter = new SimpleSpinnerAdapter(SignUpActivity.this, R.layout.row_spinner_submenu, countryList);
		spnrCountry.setAdapter(new NothingSelectedSpinnerAdapter(countryAdapter, R.layout.row_spinner_nothing_selected,
				SignUpActivity.this));
		countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		if (pdDialog.isShowing())
			pdDialog.dismiss();
	}

	private void setUpUniversities(String country) {
		ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("university");
		parseQuery.whereEqualTo("country", country);
		parseQuery.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if (e == null) {
					uniList.clear();
					if (list.size() > 0) {

						int size = list.size();
						for (int i = 0; i < size; i++) {
							uniList.add(getUniversityObjcet(list.get(i)));
						}

					}
					setUniAdapter();
				} else {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
					Constants.alert(SignUpActivity.this, "An error occured!.");

				}

			}
		});
	}

	private void selectSpiner(String name) {
		uniAdapter.notifyDataSetChanged();
	}

	private void setUniAdapter() {
		if (pdDialog.isShowing())
			pdDialog.dismiss();
		uniAdapter.notifyDataSetChanged();

	}

	private University getUniversityObjcet(ParseObject object) {
		University university = new University();
		university.setCountry_id(object.getString("country"));
		university.setUniversity_name(object.getString("university_name"));
		university.setUnivseristy_id(object.getObjectId());

		return university;
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.btn_signup) {

			String email = edtEmail.getText().toString();
			String pass = edtPass.getText().toString();
			String cpass = edtConfirmPass.getText().toString();
			String profile = edtProfileName.getText().toString();
			if (email.equals("") || pass.equals("") || cpass.equals("") || profile.equals(""))
				Constants.alert(SignUpActivity.this, "Field Empty.Please check");
			else if (!pass.equals(cpass))
				Constants.alert(SignUpActivity.this, "Password mismatch.please check");
			else if (spnrCountry.getSelectedItemPosition() <= 0)
				Constants.alert(SignUpActivity.this, "Please select a country.");
			else if (SpnrUni.getSelectedItemPosition() <= 0)
				Constants.alert(SignUpActivity.this, "Please select an institution.");
			else {
				if(Utility.hasInternet(SignUpActivity.this))
				{
					ParseUser parseUser = new ParseUser();
					parseUser.setUsername(email);
					parseUser.setEmail(email);
					parseUser.setPassword(cpass);
					parseUser.put("profile_name", edtProfileName.getText().toString());
					int position=SpnrUni.getSelectedItemPosition();
					University university=uniList.get(position-1);
					parseUser.put("university",university.getUniversity_name());
					parseUser.put("university_id", university.getUnivseristy_id());
					position=spnrCountry.getSelectedItemPosition();
					parseUser.put("country",countryList.get(position-1));
					pdDialog.show();
					parseUser.signUpInBackground(new SignUpCallback() {
	
						@Override
						public void done(ParseException e) {
							pdDialog.dismiss();
							if (e == null) {
								// take him to profile
								Intent intent=new Intent(SignUpActivity.this,NavDrawerActivity.class);
								startActivity(intent);
								overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
								sendBroadcast(new Intent(getResources().getString(R.string.intent_filter_finish)));

								
							} else {
								e.printStackTrace();
								Constants.alert(SignUpActivity.this, "Error in Sign Up.");
							}
						}
					});
				}
				else
					Constants.alert(SignUpActivity.this,"Please check your internet connection.");
			}
		}
	}

	private void showInputDialog() {
		final Dialog dialog = new Dialog(SignUpActivity.this);

		dialog.setContentView(R.layout.dialog_input_uni);
		dialog.setTitle(getResources().getString(R.string.app_name));

		final EditText edtUni = (EditText) dialog.findViewById(R.id.et_uni_name);
		Button btnSend = (Button) dialog.findViewById(R.id.btn_send);
		Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!edtUni.getText().toString().equals(""))
					postUniversityName(edtUni.getText().toString(),dialog);
					chkUni.setChecked(false);

			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				chkUni.setChecked(false);

			}
		});
		dialog.show();
	}

	private void postUniversityName(String name,final Dialog dialog) {
		final ParseObject parseObject = new ParseObject("university");
		parseObject.put("country", "Bangladesh");
		parseObject.put("university_name", name);
		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					setNewInstituitionInSpinner(parseObject);
					Toast.makeText(SignUpActivity.this,"Your institution is saved.",Toast.LENGTH_LONG).show();
					dialog.dismiss();
				}

			}
		});
	}

	private void setNewInstituitionInSpinner(ParseObject object) {
		University university = new University();
		university.setUnivseristy_id(object.getObjectId());
		university.setUniversity_name(object.getString("university_name"));
		university.setCountry_id(object.getString("country_id"));

		uniList.add(university);
		uniAdapter.notifyDataSetChanged();
		SpnrUni.setSelection(uniList.size());
	}

	public void alertCheck(Context context, String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setNeutralButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				flag = true;
			}
		});
		bld.create().show();
	}
}
