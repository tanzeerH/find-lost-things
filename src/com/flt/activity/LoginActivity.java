package com.flt.activity;


import java.util.List;

import com.findlostthings.R;
import com.flt.dao.DaoMaster;
import com.flt.dao.DaoSession;
import com.flt.dao.bookmarks;
import com.flt.dao.bookmarksDao;
import com.flt.dao.DaoMaster.OpenHelper;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends BaseActivity  implements OnClickListener{
	private EditText edtEmail,edtPass;
	private Button btnLogin,btnSignUp,btnFbLogin; 
	private ProgressDialog pdDialog;
	private DaoMaster daoMaster;
	private DaoSession daoSession;
	private bookmarksDao bmdDao;
	private SQLiteDatabase db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_in);
		btnLogin=(Button)findViewById(R.id.btn_login);
		btnSignUp=(Button)findViewById(R.id.btn_signup);
		
		edtEmail=(EditText)findViewById(R.id.et_mail);
		edtPass=(EditText)findViewById(R.id.et_pass);
		
		btnLogin.setOnClickListener(this);
		btnSignUp.setOnClickListener(this);
		
		pdDialog = new ProgressDialog(LoginActivity.this);
		pdDialog.setMessage("Please wait....");
		pdDialog.setCancelable(false);
		
		initDaos();
	}
	private void initDaos()
	{
		OpenHelper helper = new DaoMaster.DevOpenHelper(LoginActivity.this, "flt-db", null);
		db = helper.getWritableDatabase();
		daoMaster = new DaoMaster(db);
		daoSession = daoMaster.newSession();
		bmdDao=daoSession.getBookmarksDao();
	}
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_login) {
			String email=edtEmail.getText().toString();
			String pass=edtPass.getText().toString();
			if(email.equals("") || pass.equals(""))
			{
				Constants.alert(LoginActivity.this,"An Error Occured.");
			}
			else
			{
				if(Utility.hasInternet(LoginActivity.this))
				{
					pdDialog.show();
					ParseUser.logInInBackground(email,pass,new LogInCallback() {
						
						@Override
						public void done(ParseUser user, ParseException e) {
							if(e==null)
							{
								getbookMarks();
							}
							else
							{
								if(pdDialog.isShowing())
									pdDialog.dismiss();
							Constants.alert(LoginActivity.this,"An Error Occured.");
							}
							
						}
					});
				}
				else
					Constants.alert(LoginActivity.this,"Please check your internet connection.");
			}
			
		} else if (v.getId() == R.id.btn_signup) {
				
			Intent intent=new Intent(LoginActivity.this,SignUpActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			finish();
			
		}
			
		
	}
	
	private void toast(String str)
	{
		Toast.makeText(LoginActivity.this,str,Toast.LENGTH_LONG).show();
	}
	private void getbookMarks()
	{
		bmdDao.deleteAll();
		ParseQuery<ParseObject> parseQuery=ParseQuery.getQuery("bookmarks");
		parseQuery.whereEqualTo("user_id",ParseUser.getCurrentUser().getObjectId());
		
		parseQuery.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if(pdDialog.isShowing())
					pdDialog.dismiss();
				if(e==null)
				{
					int size=list.size();
					Log.e("bms size",""+size);
					for(int i=0;i<size;i++)
					{
						bookmarks bms=new bookmarks();
						bms.setBookmark_id(list.get(i).getObjectId());
						bms.setNews_id(list.get(i).getString("news_id"));
						bms.setUser_id(list.get(i).getString("user_id"));
						bmdDao.insert(bms);
					}
					Intent intent=new Intent(LoginActivity.this,NavDrawerActivity.class);
					startActivity(intent);
					overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				}
				else
					Constants.alert(LoginActivity.this,"An Error Occured.");
				
			}
		});
		
	}
	@Override
	protected void onDestroy() {
		daoMaster.getDatabase().close();
		super.onDestroy();
	}
}
