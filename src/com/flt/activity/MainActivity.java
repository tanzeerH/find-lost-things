package com.flt.activity;

import java.util.List;

import com.findlostthings.R;
import com.flt.dao.DaoMaster;
import com.flt.dao.DaoSession;
import com.flt.dao.bookmarks;
import com.flt.dao.bookmarksDao;
import com.flt.dao.DaoMaster.OpenHelper;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

	private DaoMaster daoMaster;
	private DaoSession daoSession;
	private bookmarksDao bmdDao;
	private SQLiteDatabase db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ParseUser user=ParseUser.getCurrentUser();
		Intent intent;
		initDaos();
		if(user == null)
		{
			 intent=new Intent(MainActivity.this,LoginActivity.class);
		}
		else
		{
			if(Utility.hasInternet(MainActivity.this))
				getbookMarks();
			else
				Constants.alert(MainActivity.this,"Please check your internet connection.");
			
		}
		
		
	}
	private void initDaos()
	{
		OpenHelper helper = new DaoMaster.DevOpenHelper(MainActivity.this, "flt-db", null);
		db = helper.getWritableDatabase();
		daoMaster = new DaoMaster(db);
		daoSession = daoMaster.newSession();
		bmdDao=daoSession.getBookmarksDao();
	}
	private void getbookMarks()
	{
		bmdDao.deleteAll();
		ParseQuery<ParseObject> parseQuery=ParseQuery.getQuery("bookmarks");
		parseQuery.whereEqualTo("user_id",ParseUser.getCurrentUser().getObjectId());
		
		parseQuery.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if(e==null)
				{
					int size=list.size();
					Log.e("bms size",""+size);
					for(int i=0;i<size;i++)
					{
						bookmarks bms=new bookmarks();
						bms.setBookmark_id(list.get(i).getObjectId());
						bms.setNews_id(list.get(i).getString("news_id"));
						bms.setUser_id(list.get(i).getString("user_id"));
						bmdDao.insert(bms);
					}
					Intent intent=new Intent(MainActivity.this,NavDrawerActivity.class);
					startActivity(intent);
					finish();
				}
				else
					Constants.alert(MainActivity.this,"An Error Occured.");
				
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	protected void onDestroy() {
		daoMaster.getDatabase().close();
		super.onDestroy();
	}
}
