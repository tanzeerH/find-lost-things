package com.flt.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.adapter.CategoryListAdapter;
import com.flt.adapter.GridViewAdapter;
import com.flt.adapter.ImageListAdapter;
import com.flt.adapter.SimpleListAdapter;
import com.flt.adapter.SimpleSpinnerAdapter;
import com.flt.fragment.CategoryFragment;
import com.flt.model.PostImage;
import com.flt.model.UserPicture;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.MotionEvent;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

public class PostAddActivity extends BaseActivity implements OnLongClickListener {

	private Button btnAddPhoto;
	private int TOTAL_IMAGE_COUNT = 3;
	private static final int SELECT_PICTURE = 1;
	private static final int CAMERA_REQUEST = 2;
	private String selectedImagePath;
	private Uri outputFileUri;
	private UserPicture userPic;
	private Bitmap picture;
	private ArrayList<Bitmap> imageList = new ArrayList<Bitmap>();
	private EditText edtPrice, edtDescription, edt_title;
	private ProgressDialog pdDialog;
	private Spinner spnrCat;
	private ImageView iv1, iv2, iv3;
	private SimpleSpinnerAdapter simpleSpinnerAdapter;
	private ArrayList<String> catList = new ArrayList<String>();
	private String initialImage = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post_add);
		btnAddPhoto = (Button) findViewById(R.id.btnAddPhoto);
		btnAddPhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showOptionsDialog();

			}
		});
		spnrCat = (Spinner) findViewById(R.id.sp_cat);
		edtDescription = (EditText) findViewById(R.id.edt_des);
		edt_title = (EditText) findViewById(R.id.edt_title);
		edtPrice = (EditText) findViewById(R.id.edt_price);
		iv1 = (ImageView) findViewById(R.id.iv_1);
		iv2 = (ImageView) findViewById(R.id.iv_2);
		iv3 = (ImageView) findViewById(R.id.iv_3);
		pdDialog = new ProgressDialog(PostAddActivity.this);
		pdDialog.setMessage("Please wait....");

		iv1.setOnLongClickListener(this);
		iv2.setOnLongClickListener(this);
		iv3.setOnLongClickListener(this);
		getCatStrings();
		simpleSpinnerAdapter = new SimpleSpinnerAdapter(PostAddActivity.this, R.layout.row_spinner_submenu, catList);
		spnrCat.setAdapter(simpleSpinnerAdapter);
	}

	private void getCatStrings() {

		for (int i = 0; i < CategoryFragment.catList.size(); i++) {
			catList.add(CategoryFragment.catList.get(i).getName());
		}

	}

	void prepareCamera() {
		final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
				+ "/picFolder/";
		File newdir = new File(dir);
		newdir.mkdirs();
		String file = dir + System.currentTimeMillis() + ".jpg";
		File newfile = new File(file);
		try {
			newfile.createNewFile();
		} catch (IOException e) {
		}

		outputFileUri = Uri.fromFile(newfile);

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

		startActivityForResult(cameraIntent, CAMERA_REQUEST);
	}

	private void showOptionsDialog() {
		final Dialog dialog = new Dialog(PostAddActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_attatchment);
		ArrayList<String> list = new ArrayList<String>();
		list.add("Take Photo");
		list.add("Choose from existing photos.");
		list.add("Cancel");

		ListView lv = (ListView) dialog.findViewById(R.id.lv_attatch);
		SimpleListAdapter adapter = new SimpleListAdapter(PostAddActivity.this, R.layout.row_simple_list, list);

		lv.setAdapter(adapter);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();

		// wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);
		dialog.show();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				if (position == 0) {
					prepareCamera();
				} else if (position == 1) {
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
				}
				dialog.dismiss();

			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				Uri selectedImageUri = data.getData();
				userPic = new UserPicture(selectedImageUri, getContentResolver());
				try {
					picture = userPic.getBitmap();
					if (picture != null)
						selectedImagePath = userPic.getPath();
				} catch (IOException e) {

					e.printStackTrace();
				}

				if (picture != null) {

					imageList.add(picture);
					// update views
					setUpImagesInViews();

				}
				System.out.println(selectedImagePath);
				// profilePhotoUrl = selectedImagePath;

			}
		}
		if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
			Log.e("msg", "picture saved");

			userPic = new UserPicture(outputFileUri, getContentResolver());
			try {
				picture = userPic.getBitmap();
				if (picture != null)
					selectedImagePath = userPic.getPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (picture != null) {

				imageList.add(picture);
				// update views
				setUpImagesInViews();
			} else {
				Log.e("ms", "null");
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void setUpImagesInViews() {
		int size = imageList.size();
		if (size == 1)
			iv1.setImageBitmap(imageList.get(size - 1));
		else if (size == 2)
			iv2.setImageBitmap(imageList.get(size - 1));
		else if (size == 3)
			iv3.setImageBitmap(imageList.get(size - 1));
	}

	private void postAdd() {
		int catPos = spnrCat.getSelectedItemPosition();
		String des = edtDescription.getText().toString();
		String price = edtPrice.getText().toString();
		String title = edt_title.getText().toString();
		if (title.equals(""))
			Constants.alert(PostAddActivity.this, "Please provide a title.");
		else if (catPos == 0)
			Constants.alert(PostAddActivity.this, "Please select a category.");
		else if (des.equals(""))
			Constants.alert(PostAddActivity.this, "Please provide a description.");
		else if (price.equals(""))
			Constants.alert(PostAddActivity.this, "Please provide a price.");
		else if (imageList.size() == 0)
			Constants.alert(PostAddActivity.this, "Please add at least one image.");
		else if (!Utility.hasInternet(PostAddActivity.this))
			Constants.alert(PostAddActivity.this, "Please check your internet connection.");
		else {
			// posting first add
			pdDialog.show();
			/*
			 * ByteArrayOutputStream stream = new ByteArrayOutputStream();
			 * imageList.get(0).compress(Bitmap.CompressFormat.PNG, 100,
			 * stream); saveMainImageServer(stream.toByteArray());
			 */
			postAddInServer();

		}
	}

	private void postAddInServer() {
		final ParseObject parseObject = new ParseObject("news");
		parseObject.put("user_id", ParseUser.getCurrentUser().getObjectId());
		parseObject.put("price", edtPrice.getText().toString());
		parseObject.put("title", edt_title.getText().toString());
		parseObject.put("profile_name", ParseUser.getCurrentUser().getString("profile_name"));
		parseObject.put("university", ParseUser.getCurrentUser().getString("university"));
		parseObject.put("university_id", ParseUser.getCurrentUser().getString("university_id"));
		int selectedCat = spnrCat.getSelectedItemPosition();
		Log.e("selected cat", "" + selectedCat);
		String catObjectId = CategoryFragment.catList.get(selectedCat).getObjectId();
		Log.e("selected cat id", "" + catObjectId);
		parseObject.put("category", catList.get(selectedCat));
		parseObject.put("category_id", catObjectId);
		parseObject.put("description", edtDescription.getText().toString());
		parseObject.put("status", 0);

		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					String id = parseObject.getObjectId();
					postImagesInserver(parseObject.getObjectId());
				} else {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
					Constants.alert(PostAddActivity.this, "An error occured.\n" + e.getMessage());
				}

			}
		});
	}

	private void postImagesInserver(String objectId) {
		for (int i = 0; i < imageList.size(); i++) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			imageList.get(i).compress(Bitmap.CompressFormat.PNG, 100, stream);
			saveImageServer(stream.toByteArray(), objectId, i);
		}
	}

	private void saveImageServer(byte[] bytes, final String objectId, final int imagePosition) {

		String filename = ParseUser.getCurrentUser().getObjectId() + System.currentTimeMillis() + "-picture.jpg";
		final ParseFile parseFile = new ParseFile(filename, bytes);
		/*
		 * parseFile.saveInBackground(new SaveCallback() {
		 * 
		 * @Override public void done(ParseException e) { if (e == null) {
		 * Log.e("PIC URL", parseFile.getUrl()); if(imagePosition==0)
		 * initialImage=parseFile.getUrl();
		 * 
		 * saveImageObjectInServer(objectId,parseFile.getUrl(),imagePosition);
		 * 
		 * } else { if(pdDialog.isShowing()) pdDialog.dismiss();
		 * Log.e("PIC URL Error", "error"+e.getMessage()); }
		 * 
		 * } });
		 */
		final ParseObject parseObject = new ParseObject("images");

		parseObject.put("image_file", parseFile);
		parseObject.put("add_id", objectId);
		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Log.e("PIC URL", parseFile.getUrl());
					if (imagePosition == 0)
						initialImage = parseObject.getParseFile("image_file").getUrl();
					if(imagePosition==(imageList.size()-1))
						postImaageUrl(objectId);
				} else {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
					Log.e("PIC URL Error", "error" + e.getMessage());
				}

			}
		});
	}

	private void saveImageObjectInServer(final String addId, String imageUrl, final int imagePosition) {
		final ParseObject parseObject = new ParseObject("images");

		parseObject.put("image_url", imageUrl);
		parseObject.put("add_id", addId);

		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					/*
					 * if(pdDialog.isShowing()) pdDialog.dismiss(); String
					 * id=parseObject.getObjectId();
					 */
					if (imagePosition == imageList.size() - 1)
						postImaageUrl(addId);
				} else {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
					Constants.alert(PostAddActivity.this, "An error occured.\n" + e.getMessage());
				}

			}
		});

	}

	private void postImaageUrl(String newsId) {
		ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("news");
		parseQuery.getInBackground(newsId, new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject parseObject, ParseException e) {
				if (pdDialog.isShowing())
					pdDialog.dismiss();
				parseObject.put("image_url", initialImage);
				parseObject.saveInBackground();
				if (e == null) {
					Constants.alert(PostAddActivity.this, "Advertisement posted successfully.");
				}

			}
		});
	}

	@Override
	public boolean onLongClick(View v) {
		if (v.getId() == R.id.iv_1) {
			if (imageList.size() >= 1) {
				showDeleteAlert("Do you want to delete this image?", 0);
			}
		} else if (v.getId() == R.id.iv_2) {
			if (imageList.size() >= 2) {
				showDeleteAlert("Do you want to delete this image?", 1);
			}
		} else if (v.getId() == R.id.iv_3) {
			if (imageList.size() >= 3) {
				showDeleteAlert("Do you want to delete this image?", 2);
			}
		}
		return false;
	}

	private void showDeleteAlert(String message, final int imagePosition) {
		AlertDialog.Builder bld = new AlertDialog.Builder(PostAddActivity.this, AlertDialog.THEME_HOLO_LIGHT);
		// bld.setTitle(context.getResources().getText(R.string.app_name));
		bld.setTitle(R.string.app_name);
		bld.setMessage(message);
		bld.setCancelable(false);
		bld.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				rebuildImageViews(imagePosition);

			}
		});
		bld.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();

			}
		});

		bld.create().show();
	}

	private void rebuildImageViews(int position) {
		imageList.remove(position);
		for (int i = 0; i < imageList.size(); i++) {
			if (i == 0)
				iv1.setImageBitmap(imageList.get(i));
			else if (i == 1)
				iv2.setImageBitmap(imageList.get(i));
			else if (i == 2)
				iv3.setImageBitmap(imageList.get(i));
		}
		for (int i = imageList.size(); i < TOTAL_IMAGE_COUNT; i++) {
			if (i == 0)
				iv1.setImageResource(R.drawable.placeholder);
			else if (i == 1)
				iv2.setImageResource(R.drawable.placeholder);
			else if (i == 2)
				iv3.setImageResource(R.drawable.placeholder);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.post_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_post) {
			// post the news
			postAdd();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
