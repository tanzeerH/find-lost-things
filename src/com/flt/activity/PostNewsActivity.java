package com.flt.activity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.findlostthings.R;
import com.flt.adapter.GridViewAdapter;
import com.flt.adapter.ImageListAdapter;
import com.flt.adapter.SimpleListAdapter;
import com.flt.model.PostImage;
import com.flt.model.UserPicture;
import com.flt.utility.Constants;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;

public class PostNewsActivity extends Activity {
	private int width;
	private ListView listView;
	private Button btnAddPhoto;
	private static final int SELECT_PICTURE = 1;
	private static final int CAMERA_REQUEST = 2;
	private String selectedImagePath;
	private Uri outputFileUri;
	private UserPicture userPic;
	private Bitmap picture;
	private ArrayList<Bitmap> imageList = new ArrayList<Bitmap>();
	
	private GridViewAdapter gAdapter ;
	private EditText edtTitle,edtType,edtDescription,edtLocation;
	private ProgressDialog pdDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post_lost_news);
		btnAddPhoto = (Button) findViewById(R.id.btnAddPhoto);
		btnAddPhoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showImageDialog();
				
			}
		});
		edtTitle=(EditText)findViewById(R.id.etTitle);
		edtType=(EditText)findViewById(R.id.etItemType);
		edtDescription=(EditText)findViewById(R.id.etDescription);
		edtLocation=(EditText)findViewById(R.id.etLocation);
		pdDialog=new ProgressDialog(PostNewsActivity.this);
		pdDialog.setMessage("Please wait....");
		getWidth();

	}

	void prepareCamera() {
		final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
				+ "/picFolder/";
		File newdir = new File(dir);
		newdir.mkdirs();
		String file = dir + System.currentTimeMillis() + ".jpg";
		File newfile = new File(file);
		try {
			newfile.createNewFile();
		} catch (IOException e) {
		}

		outputFileUri = Uri.fromFile(newfile);

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

		startActivityForResult(cameraIntent, CAMERA_REQUEST);
	}

	private void getWidth() {
		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
		width = displayMetrics.widthPixels;
	}

	private void showOptionsDialog() {
		final Dialog dialog = new Dialog(PostNewsActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_attatchment);
		ArrayList<String> list = new ArrayList<String>();
		list.add("Take Photo");
		list.add("Choose from existing photos.");
		list.add("Cancel");

		ListView lv = (ListView) dialog.findViewById(R.id.lv_attatch);
		SimpleListAdapter adapter = new SimpleListAdapter(PostNewsActivity.this, R.layout.row_simple_list, list);

		lv.setAdapter(adapter);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();

		// wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);
		dialog.show();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				if (position == 0) {
					prepareCamera();
				} else if (position == 1) {
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
				}
				dialog.dismiss();

			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				Uri selectedImageUri = data.getData();
				userPic = new UserPicture(selectedImageUri, getContentResolver());
				try {
					picture = userPic.getBitmap();
					if (picture != null)
						selectedImagePath = userPic.getPath();
				} catch (IOException e) {

					e.printStackTrace();
				}

				if (picture != null) {

					imageList.add(picture);
					gAdapter.notifyDataSetChanged();

				}
				System.out.println(selectedImagePath);
				// profilePhotoUrl = selectedImagePath;

			}
		}
		if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
			Log.e("msg", "picture saved");

			userPic = new UserPicture(outputFileUri, getContentResolver());
			try {
				picture = userPic.getBitmap();
				if (picture != null)
					selectedImagePath = userPic.getPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (picture != null) {

				imageList.add(picture);
				gAdapter.notifyDataSetChanged();
			} else {
				Log.e("ms", "null");
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void showImageDialog() {
		final Dialog dialog = new Dialog(PostNewsActivity.this);
		dialog.setContentView(R.layout.dialog_photos);
		dialog.setTitle("Images");
		GridView gv = (GridView) dialog.findViewById(R.id.gvPhotos);
		Button btnAddPhoto=(Button)dialog.findViewById(R.id.btn_add_photo);
		btnAddPhoto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showOptionsDialog();
				
				
			}
		});
		gAdapter = new GridViewAdapter(PostNewsActivity.this, width / 2, imageList);
		gv.setAdapter(gAdapter);
		dialog.show();

	}
	private void postNews()
	{
		pdDialog.show();
		ParseUser user=ParseUser.getCurrentUser();
		String user_id=user.getObjectId();
		Log.e("user_id",""+user_id);
		
		ParseObject parseObject=new ParseObject("news");
		parseObject.put("user_id",user_id);
		parseObject.put("username",user.getUsername());
		parseObject.put("status",0);
		parseObject.put("title",edtTitle.getText().toString());
		parseObject.put("item_type",edtType.getText().toString());
		parseObject.put("description",edtDescription.getText().toString());
		parseObject.put("location",edtLocation.getText().toString());
		parseObject.saveInBackground(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				pdDialog.dismiss();
				if(e==null)
				{
					Constants.alert(PostNewsActivity.this,"News is posted.");
					
				}
				else
				{
					e.printStackTrace();
					
					Constants.alert(PostNewsActivity.this,"An error occured.\n"+e.getMessage());
				}
				
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.post_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_post) {
			//post the news
			postNews();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
