package com.flt.activity;

import java.util.ArrayList;

import com.findlostthings.R;
import com.flt.adapter.NavDrawerAdapter;
import com.flt.fragment.AdminFragment;
import com.flt.fragment.BookMarksFragment;
import com.flt.fragment.CategoryFragment;
import com.flt.fragment.ChatListFragment;
import com.flt.fragment.MyPostsFragment;
import com.flt.fragment.NewsFargment;
import com.flt.fragment.ProfileFragment;
import com.flt.model.NavMenuItem;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class NavDrawerActivity extends Activity {
	private ActionBarDrawerToggle mDrawerToggle;
	private NavDrawerAdapter navDrawerAdapter;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ArrayList<NavMenuItem> drawerItemList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nav_drawer);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		mDrawerList.setFitsSystemWindows(true);
		if(getActionBar()!=null)
		{
		getActionBar().setTitle("");
		getActionBar().setIcon(null);
		getActionBar().setLogo(null);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		}
		setDrawerListItems();
		setParseInstallation();
		navDrawerAdapter=new NavDrawerAdapter(NavDrawerActivity.this,R.layout.row_nav_menu,drawerItemList);
		mDrawerList.setAdapter(navDrawerAdapter);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_nav, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);	
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
				if(position==0)
				{
					CategoryFragment categoryFragment=new CategoryFragment();
					fragmentTransaction.replace(R.id.frame_container,categoryFragment);
				}
				else if(position==1)
				{
					ProfileFragment profileFragment=new ProfileFragment();
					fragmentTransaction.replace(R.id.frame_container,profileFragment);
				}
				else if(position==2)
				{
					ChatListFragment chatListFragment=new ChatListFragment();
					fragmentTransaction.replace(R.id.frame_container,chatListFragment);
				}
				else if(position==3)
				{
					AdminFragment adminFragment=new AdminFragment();
					fragmentTransaction.replace(R.id.frame_container,adminFragment);
				}
				else if(position==4)
				{
					MyPostsFragment myPostsFragment=new MyPostsFragment();
					fragmentTransaction.replace(R.id.frame_container,myPostsFragment);
				}
				else if(position==5)
				{
					BookMarksFragment bookMarksFragment=new BookMarksFragment();
					fragmentTransaction.replace(R.id.frame_container,bookMarksFragment);
				}
				else if(position==6)
				{
					ParseUser.getCurrentUser().logOut();
					Intent intent=new Intent(NavDrawerActivity.this,LoginActivity.class);
					startActivity(intent);
					finish();
				}
				mDrawerLayout.closeDrawer(Gravity.LEFT);
				fragmentTransaction.commit();
			}
		});
		
		FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
		CategoryFragment categoryFragment=new CategoryFragment();
		fragmentTransaction.replace(R.id.frame_container,categoryFragment);
		fragmentTransaction.commit();
		
	}
	private void setParseInstallation()
	{
		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
		installation.put("user",ParseUser.getCurrentUser().getObjectId());
		installation.saveInBackground(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				if(e==null)
				{
					
				}
				else
				{
					Log.e("msg","error");
					e.printStackTrace();
				}
				
			}
		});
	}
	private void setDrawerListItems()
	{
		drawerItemList=new ArrayList<NavMenuItem>();
		drawerItemList.add(new NavMenuItem("Category",R.drawable.ic_launcher));
		drawerItemList.add(new NavMenuItem("Profile",R.drawable.ic_launcher));
		drawerItemList.add(new NavMenuItem("Chat History",R.drawable.ic_launcher));
		drawerItemList.add(new NavMenuItem("Admin",R.drawable.ic_launcher));
		drawerItemList.add(new NavMenuItem("My Posts",R.drawable.ic_launcher));
		drawerItemList.add(new NavMenuItem("Bookmarks",R.drawable.ic_launcher));
		drawerItemList.add(new NavMenuItem("Logout",R.drawable.ic_launcher));
		
	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		return super.onPrepareOptionsMenu(menu);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.news_menu, menu);
		return true;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		int id = item.getItemId();
		if (id == R.id.action_add) {
			Intent i=new Intent(NavDrawerActivity.this,PostAddActivity.class);
			startActivity(i);
			return true;
		}
		return true;
	}
}
