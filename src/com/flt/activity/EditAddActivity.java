package com.flt.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.findlostthings.R;
import com.flt.adapter.CategoryListAdapter;
import com.flt.adapter.GridViewAdapter;
import com.flt.adapter.ImageListAdapter;
import com.flt.adapter.SimpleListAdapter;
import com.flt.adapter.SimpleSpinnerAdapter;
import com.flt.fragment.CategoryFragment;
import com.flt.lazylist.ImageLoader;
import com.flt.model.News;
import com.flt.model.NewsImage;
import com.flt.model.PostImage;
import com.flt.model.UserPicture;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;

public class EditAddActivity extends BaseActivity {

	private Button btnAddPhoto;
	private static final int SELECT_PICTURE = 1;
	private static final int CAMERA_REQUEST = 2;
	private String selectedImagePath;
	private Uri outputFileUri;
	private UserPicture userPic;
	private Bitmap picture;
	private EditText edtPrice, edtDescription, edt_title;
	private ProgressDialog pdDialog;
	private Spinner spnrCat;
	private ImageView iv1, iv2, iv3;
	private SimpleSpinnerAdapter simpleSpinnerAdapter;
	private ArrayList<String> catList = new ArrayList<String>();
	private News selectedNews;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post_add);
		btnAddPhoto = (Button) findViewById(R.id.btnAddPhoto);
		btnAddPhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (selectedNews.getImageList().size() >= 3)
					Constants.alert(EditAddActivity.this, "You can upload maximum 3 images.");
				else
					showOptionsDialog();

			}
		});
		spnrCat = (Spinner) findViewById(R.id.sp_cat);
		edtDescription = (EditText) findViewById(R.id.edt_des);
		edt_title = (EditText) findViewById(R.id.edt_title);
		edtPrice = (EditText) findViewById(R.id.edt_price);
		iv1 = (ImageView) findViewById(R.id.iv_1);
		iv2 = (ImageView) findViewById(R.id.iv_2);
		iv3 = (ImageView) findViewById(R.id.iv_3);
		pdDialog = new ProgressDialog(EditAddActivity.this);
		pdDialog.setMessage("Please wait....");
		selectedNews = Constants.selectedNews;
		getCatStrings();
		simpleSpinnerAdapter = new SimpleSpinnerAdapter(EditAddActivity.this, R.layout.row_spinner_submenu, catList);
		spnrCat.setAdapter(simpleSpinnerAdapter);
		setUpViews();
	}

	private void getCatStrings() {

		for (int i = 0; i < CategoryFragment.catList.size(); i++) {
			catList.add(CategoryFragment.catList.get(i).getName());
		}

	}

	private void setUpViews() {
		edt_title.setText(selectedNews.getTitle());
		edtDescription.setText(selectedNews.getDescription());
		edtPrice.setText("" + selectedNews.getPrice());
		setUpImages();
	}

	private void setUpImages() {
		ImageLoader imageLoader = new ImageLoader(EditAddActivity.this);
		int size = selectedNews.getImageList().size();
		for (int i = 0; i < size; i++) {
			if (i == 0)
				imageLoader.DisplayImage(selectedNews.getImageList().get(i).getImage_url(), iv1);
			else if (i == 1)
				imageLoader.DisplayImage(selectedNews.getImageList().get(i).getImage_url(), iv2);
			else if (i == 2)
				imageLoader.DisplayImage(selectedNews.getImageList().get(i).getImage_url(), iv3);
		}
	}

	void prepareCamera() {
		final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
				+ "/picFolder/";
		File newdir = new File(dir);
		newdir.mkdirs();
		String file = dir + System.currentTimeMillis() + ".jpg";
		File newfile = new File(file);
		try {
			newfile.createNewFile();
		} catch (IOException e) {
		}

		outputFileUri = Uri.fromFile(newfile);

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

		startActivityForResult(cameraIntent, CAMERA_REQUEST);
	}

	private void showOptionsDialog() {
		final Dialog dialog = new Dialog(EditAddActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_attatchment);
		ArrayList<String> list = new ArrayList<String>();
		list.add("Take Photo");
		list.add("Choose from existing photos.");
		list.add("Cancel");

		ListView lv = (ListView) dialog.findViewById(R.id.lv_attatch);
		SimpleListAdapter adapter = new SimpleListAdapter(EditAddActivity.this, R.layout.row_simple_list, list);

		lv.setAdapter(adapter);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();

		// wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);
		dialog.show();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				if (position == 0) {
					prepareCamera();
				} else if (position == 1) {
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
				}
				dialog.dismiss();

			}
		});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				Uri selectedImageUri = data.getData();
				userPic = new UserPicture(selectedImageUri, getContentResolver());
				try {
					picture = userPic.getBitmap();
					if (picture != null)
						selectedImagePath = userPic.getPath();
				} catch (IOException e) {

					e.printStackTrace();
				}

				if (picture != null) {

					// update views
					postImageInServer(picture, selectedNews.getNews_object_id());

				}
				System.out.println(selectedImagePath);
				// profilePhotoUrl = selectedImagePath;

			}
		}
		if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
			Log.e("msg", "picture saved");

			userPic = new UserPicture(outputFileUri, getContentResolver());
			try {
				picture = userPic.getBitmap();
				if (picture != null)
					selectedImagePath = userPic.getPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (picture != null) {

				// update views
				postImageInServer(picture, selectedNews.getNews_object_id());
			} else {
				Log.e("ms", "null");
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void setUpImagesInViews() {
		/*
		 * int size = imageList.size(); if (size == 1)
		 * iv1.setImageBitmap(imageList.get(size - 1)); else if (size == 2)
		 * iv2.setImageBitmap(imageList.get(size - 1)); else if (size == 3)
		 * iv3.setImageBitmap(imageList.get(size - 1));
		 */
	}

	private void uploadImagesInServer() {

	}

	private void updateAdd() {
		int catPos = spnrCat.getSelectedItemPosition();
		String des = edtDescription.getText().toString();
		String price = edtPrice.getText().toString();
		String title = edt_title.getText().toString();
		if (title.equals(""))
			Constants.alert(EditAddActivity.this, "Please provide a title.");
		else if (catPos == 0)
			Constants.alert(EditAddActivity.this, "Please select a category.");
		else if (des.equals(""))
			Constants.alert(EditAddActivity.this, "Please provide a description.");
		else if (price.equals(""))
			Constants.alert(EditAddActivity.this, "Please provide a price.");
		else if (selectedNews.getImageList().size() == 0)
			Constants.alert(EditAddActivity.this, "Please select at least one image.");
		else if (!Utility.hasInternet(EditAddActivity.this))
			Constants.alert(EditAddActivity.this, "Please check your internet connection.");
		else {
			// posting first add
			if (Utility.hasInternet(EditAddActivity.this)) {
				pdDialog.show();
				/*
				 * ByteArrayOutputStream stream = new ByteArrayOutputStream();
				 * imageList.get(0).compress(Bitmap.CompressFormat.PNG, 100,
				 * stream); saveMainImageServer(stream.toByteArray());
				 */
				updateAddInServer();
			} else
				Constants.alert(EditAddActivity.this, "Please check your internet connection.");

		}
	}

	private void updateAddInServer() {

		ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("news");
		parseQuery.getInBackground(selectedNews.getNews_object_id(), new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject parseObject, ParseException e) {
				if (e == null) {
					parseObject.put("user_id", ParseUser.getCurrentUser().getObjectId());
					parseObject.put("price", edtPrice.getText().toString());
					parseObject.put("title", edt_title.getText().toString());
					parseObject.put("profile_name", ParseUser.getCurrentUser().getString("profile_name"));
					parseObject.put("university", ParseUser.getCurrentUser().getString("university"));
					parseObject.put("university_id", ParseUser.getCurrentUser().getString("university_id"));
					int selectedCat = spnrCat.getSelectedItemPosition();
					Log.e("selected cat", "" + selectedCat);
					String catObjectId = CategoryFragment.catList.get(selectedCat).getObjectId();
					Log.e("selected cat id", "" + catObjectId);
					parseObject.put("category", catList.get(selectedCat));
					parseObject.put("category_id", catObjectId);
					parseObject.put("description", edtDescription.getText().toString());
					parseObject.put("status", 0);
					parseObject.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							if (e == null) {
								Constants.alert(EditAddActivity.this, "Add updated.");
							} else {
								if (pdDialog.isShowing())
									pdDialog.dismiss();
								Constants.alert(EditAddActivity.this, "An error occured.\n" + e.getMessage());
							}

						}
					});
				}

			}
		});

	}

	private void postImageInServer(Bitmap bitmap, String objectId) {
		if (Utility.hasInternet(EditAddActivity.this)) {
			pdDialog.show();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
			saveImageServer(stream.toByteArray(), objectId);
		} else
			Constants.alert(EditAddActivity.this, "Please check your internet connection.");
	}

	private void saveImageServer(byte[] bytes, final String objectId) {

		String filename = ParseUser.getCurrentUser().getObjectId() + System.currentTimeMillis() + "-picture.jpg";
		 ParseFile parseFile = new ParseFile(filename, bytes);
		/*parseFile.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Log.e("PIC URL", parseFile.getUrl());
					saveImageObjectInServer(objectId, parseFile.getUrl());

				} else {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
					Log.e("PIC URL Error", "error" + e.getMessage());
				}

			}
		});*/
		final ParseObject parseObject = new ParseObject("images");

		parseObject.put("image_file", parseFile);
		parseObject.put("add_id", objectId);
		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
				} else {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
					Log.e("PIC URL Error", "error" + e.getMessage());
				}

			}
		});
	}

	private void saveImageObjectInServer(String addId, final String imageUrl) {
		final ParseObject parseObject = new ParseObject("images");

		parseObject.put("image_url", imageUrl);
		parseObject.put("add_id", addId);

		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
					String id = parseObject.getObjectId();
					NewsImage newsImage = new NewsImage();
					newsImage.setObjectId(id);
					newsImage.setImage_url(imageUrl);
					selectedNews.getImageList().add(newsImage);
					setUpImages();
				} else {
					if (pdDialog.isShowing())
						pdDialog.dismiss();
					Constants.alert(EditAddActivity.this, "An error occured.\n" + e.getMessage());
				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.post_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_post) {
			// post the news
			updateAdd();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
