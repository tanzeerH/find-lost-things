package com.flt.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.findlostthings.R;
import com.flt.adapter.MessageListAdapter;
import com.flt.fltbroadcastreceiver.PushNotificationReceiver;
import com.flt.model.Message;
import com.flt.model.News;
import com.flt.utility.Constants;
import com.flt.utility.PreferenceConnector;
import com.flt.utility.Utility;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class MessageActivity extends BaseActivity {

	private ListView lvMessages;
	private PullToRefreshListView pullLvMessages;
	private EditText etMsg;
	private TextView tvSend;
	private MessageListAdapter messageListAdapter;
	private ArrayList<Message> msgList = new ArrayList<Message>();
	String news_poster_id;
	String news_postr_user_name;
	String cur_user_id;
	String cur_user_name;
	String chatId = "";
	boolean pullUp=true;
	private boolean refreshFlag = false;
	PushNotificationReceiver pushNotificationReceiver = new PushNotificationReceiver() {
		public void onReceive(android.content.Context context, android.content.Intent intent) {

			Log.e("push received", "push message");
			getMessage();
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
		pullLvMessages = (PullToRefreshListView) findViewById(R.id.lv_messages);
		pullLvMessages.setMode(Mode.BOTH);
		pullLvMessages.setShowIndicator(false);
		lvMessages = this.pullLvMessages.getRefreshableView();
		pullLvMessages.setOnRefreshListener(new OnRefreshListener2() {

			@Override
			public void onPullDownToRefresh(PullToRefreshBase refreshView) {
				refreshFlag = true;
				getMessage();
				
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase refreshView) {
				refreshFlag = true;
				pullUp=true;
				getMessage();
				
			}
		});
		
		etMsg = (EditText) findViewById(R.id.et_msg);
		tvSend = (TextView) findViewById(R.id.tv_send);
		tvSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				postMessage();
			}
		});

		setUp();
	}

	@Override
	protected void onResume() {
		registerReceiver(pushNotificationReceiver, new IntentFilter("com.flt.push"));
		if (chatId != null && !chatId.equals(""))
			PreferenceConnector.writeString(MessageActivity.this, PreferenceConnector.KEY_CURRENT_THREAD_ID, chatId);
		super.onResume();
	}

	private void setUp() {
		Intent i = getIntent();
		news_poster_id = i.getStringExtra("news_poster_id");
		news_postr_user_name = i.getStringExtra("news_poster_name");
		cur_user_id = ParseUser.getCurrentUser().getObjectId();
		cur_user_name = ParseUser.getCurrentUser().getString("profile_name");
		
		chatId = i.getStringExtra("chat_id");

		if (chatId == null)
			checkChatThread(1);
		else
		{
			PreferenceConnector.writeString(MessageActivity.this, PreferenceConnector.KEY_CURRENT_THREAD_ID, chatId);
			getMessage();
		}
	}

	private void postMessage() {
		if (Utility.hasInternet(MessageActivity.this)) {
			String msg = etMsg.getText().toString();
			if (!msg.equals("")) {
				ParseObject parseObject = new ParseObject("messages");
				parseObject.put("sender", ParseUser.getCurrentUser().getObjectId());
				parseObject.put("chat_id", chatId);
				parseObject.put("message", msg);
				parseObject.saveInBackground(new SaveCallback() {

					@Override
					public void done(ParseException e) {
						if (e == null) {
							// Constants.alert(MessageActivity.this,"message saved.");

							sendPush(etMsg.getText().toString());
							etMsg.setText("");
							getMessage();
						}

					}
				});
			}
			else
				Constants.alert(MessageActivity.this, "Please check your internet connection.");
		}
	}

	private void sendPush(String message) {
		ParseQuery pushQuery = ParseInstallation.getQuery();

		pushQuery.whereEqualTo("user", news_poster_id);
		Log.e("news_poster_id", news_poster_id);
		ParsePush parsePush = new ParsePush();
		parsePush.setQuery(pushQuery);
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("message", message);
			jsonObject.put("chat_id", chatId);
			jsonObject.put("sender", ParseUser.getCurrentUser().getObjectId());
			jsonObject.put("profile_name", ParseUser.getCurrentUser().getString("profile_name"));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		parsePush.setData(jsonObject);
		parsePush.sendInBackground(new SendCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Log.e("PUSH", "successfull");
				} else
					e.printStackTrace();

			}
		});

	}

	private void getMessage() {
		if (Utility.hasInternet(MessageActivity.this)) {
			ParseQuery<ParseObject> query = ParseQuery.getQuery("messages");
			query.whereEqualTo("chat_id", chatId);
			if (msgList.size() > 0  && !pullUp) {
				int size = msgList.size();
				Date lastestDate = msgList.get(0).getCreatedDate();
				query.whereGreaterThan("createdAt", lastestDate);
			}
			query.orderByDescending("createdAt");
			query.setLimit(10);
			query.findInBackground(new FindCallback<ParseObject>() {

				@Override
				public void done(List<ParseObject> list, ParseException e) {
					if (e == null) {
						Log.e("msg size", "" + list.size());
						if(pullUp)
						{
							msgList.clear();
							pullUp=false;
						}
						if (list.size() > 0) {

							Collections.reverse(list);
							ArrayList<Message> tempList=new ArrayList<Message>();
							for (int i = 0; i < list.size(); i++)
								tempList.add(getMsgObjcet(list.get(i)));
							if (refreshFlag) {
								pullLvMessages.onRefreshComplete();
								refreshFlag = false;
							} else {
								int size = msgList.size();
								lvMessages.setSelection(size - 1);
								lvMessages.smoothScrollToPosition(size - 1);
							}
							tempList.addAll(msgList);
							msgList=tempList;
							messageListAdapter = new MessageListAdapter(MessageActivity.this,
									R.layout.row_message_send, msgList);
							lvMessages.setAdapter(messageListAdapter);

						} else {
							pullLvMessages.onRefreshComplete();
							refreshFlag = false;
						}
					} else {
						pullLvMessages.onRefreshComplete();
						refreshFlag = false;
					}
				}
			});
		} else
			Constants.alert(MessageActivity.this, "Please check your internet connection.");
	}

	private Message getMsgObjcet(ParseObject object) {
		Message message = new Message();
		message.setObjectId(object.getObjectId());
		message.setMessage(object.getString("message"));
		message.setSenderId(object.getString("sender"));
		message.setCreatedDate(object.getCreatedAt());
		// news.setDateFound(""+object.get)
		return message;

	}

	private void checkChatThread(final int flag) {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("chats");
		if (flag == 1) {
			query.whereEqualTo("user1", news_poster_id);
			query.whereEqualTo("user2", cur_user_id);
		} else if (flag == 2) {
			query.whereEqualTo("user1", cur_user_id);
			query.whereEqualTo("user2", news_poster_id);
		}

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if (e == null) {
					if (list.size() == 0) {
						if (flag == 1)
							checkChatThread(2);
						else if (flag == 2)
							createChatThread();
					} else {
						chatId = list.get(0).getObjectId();
						PreferenceConnector.writeString(MessageActivity.this,
								PreferenceConnector.KEY_CURRENT_THREAD_ID, chatId);
						Constants.alert(MessageActivity.this, "already have a chat thread.id="
								+ list.get(0).getObjectId());
						getMessage();
					}

				}

			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();
		PreferenceConnector.writeString(MessageActivity.this, PreferenceConnector.KEY_CURRENT_THREAD_ID, "-1");
	}

	private void createChatThread() {
		final ParseObject parseObject = new ParseObject("chats");
		parseObject.put("user1", news_poster_id);
		parseObject.put("user1_name", news_postr_user_name);
		parseObject.put("user2", cur_user_id);
		parseObject.put("user2_name", cur_user_name);
		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					PreferenceConnector.writeString(MessageActivity.this, PreferenceConnector.KEY_CURRENT_THREAD_ID,
							chatId);
					chatId = parseObject.getObjectId();
					getMessage();
					Constants.alert(MessageActivity.this, "Chat thread created.id=" + parseObject.getObjectId());

				}

			}
		});

	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(pushNotificationReceiver);
		super.onDestroy();
	}

}
