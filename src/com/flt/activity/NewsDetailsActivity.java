package com.flt.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import com.findlostthings.R;
import com.flt.adapter.CommentListAdapter;
import com.flt.adapter.ImagePagerAdapter;
import com.flt.dao.DaoMaster;
import com.flt.dao.DaoMaster.OpenHelper;
import com.flt.dao.DaoSession;
import com.flt.dao.bookmarks;
import com.flt.dao.bookmarksDao;
import com.flt.fragment.ImageFragment;
import com.flt.model.Comment;
import com.flt.model.News;
import com.flt.model.NewsImage;
import com.flt.utility.Constants;
import com.flt.utility.PreferenceConnector;
import com.flt.utility.Utility;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.viewpagerindicator.CirclePageIndicator;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class NewsDetailsActivity extends FragmentActivity {
	ArrayList<ImageFragment> productImageFragment=new ArrayList<ImageFragment>();
	public ViewPager mPager;
	private CirclePageIndicator indicator;
	private ImagePagerAdapter mPagerAdapter;
	public static int pageNumber;
	private ArrayList<NewsImage> imageList=new ArrayList<NewsImage>();
	private ProgressDialog pdDialog;
	private TextView tvTitle,tvDate,tvStatus,tvDescription,tvProfile,tvPrice,tvUni,tvComment,tvMessage;
	private ImageView iv_Bookmarked;
	
	private ArrayList<Comment> cmntList = new ArrayList<Comment>();
	CommentListAdapter commentListAdapter;
	private List<String> bookMarksList;
	private News news=null;
	private DaoMaster daoMaster;
	private DaoSession daoSession;
	private bookmarksDao bmdDao;
	
	private SQLiteDatabase db;
	private List<bookmarks> bmdList;
	
	BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			finish();

		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_details);
		mPager = (ViewPager) findViewById(R.id.pager);
		indicator = (CirclePageIndicator) findViewById(R.id.indicator);
		tvComment=(TextView)findViewById(R.id.tv_comments);
		tvMessage=(TextView)findViewById(R.id.tv_message);
		tvTitle=(TextView)findViewById(R.id.tv_title);
		tvProfile=(TextView)findViewById(R.id.tv_profile_name);
		tvUni=(TextView)findViewById(R.id.tv_uni_name);
		tvPrice=(TextView)findViewById(R.id.tv_price);
		tvDate=(TextView)findViewById(R.id.tv_date);
		tvStatus=(TextView)findViewById(R.id.tv_status);
		tvDescription=(TextView)findViewById(R.id.tv_description);
		iv_Bookmarked=(ImageView)findViewById(R.id.iv_bookmark);
		
		news=Constants.selectedNews;
		initDaos();
		setUpViews();
		pdDialog=new ProgressDialog(NewsDetailsActivity.this);
		pdDialog.setMessage("Please wait....");
		if(Utility.hasInternet(NewsDetailsActivity.this))
			getImages();
		else
			Constants.alert(NewsDetailsActivity.this,"Please check your internet connection.");
	}
	@Override
	protected void onResume() {
		registerReceiver(broadcastReceiver, new IntentFilter(getResources().getString(R.string.intent_filter_finish)));
		super.onResume();
	}

	private void setUpViews()
	{
		
		tvTitle.setText(news.getTitle());
		tvDescription.setText(news.getDescription());
		tvDate.setText(news.getCreated_at());
		tvPrice.setText(news.getPrice()+" TK");
		tvProfile.setText(news.getProfile_name());
		tvUni.setText(news.getUniversity_name());
		
		tvComment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(Utility.hasInternet(NewsDetailsActivity.this))
					showCommentDialog(news);
				else
					Constants.alert(NewsDetailsActivity.this,"Please check your internet connection.");
				
			}
		});
		if(ParseUser.getCurrentUser().getObjectId().equals(news.getUserObjectId()))
		{
			tvMessage.setVisibility(View.INVISIBLE);
		}
		else
		{
			tvMessage.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(NewsDetailsActivity.this, MessageActivity.class);
					intent.putExtra("news_poster_name",Constants.selectedNews.getProfile_name());
					intent.putExtra("news_poster_id",Constants.selectedNews.getUserObjectId());
					startActivity(intent);
					overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
					
				}
			});
			
		}
		iv_Bookmarked.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.e("is book marked previous", ""+IsBookmarked(news.getNews_object_id()+""));
				if(IsBookmarked(news.getNews_object_id()))
				{
					iv_Bookmarked.setImageResource(R.drawable.ic_not_bookmarked);
					final bookmarks bms=getBookMarkedObject(news.getNews_object_id());
					ParseQuery<ParseObject> parseQuery=ParseQuery.getQuery("bookmarks");
					parseQuery.getInBackground(bms.getBookmark_id(),new GetCallback<ParseObject>() {
						
						@Override
						public void done(ParseObject parseObject, ParseException e) {
							if(e==null)
							{
								bmdDao.delete(bms);
								parseObject.deleteInBackground();
							}
								
							
						}
					});
					
				}
				else
				{
				iv_Bookmarked.setImageResource(R.drawable.ic_bookmarked);
				
				final ParseObject parseObject=new ParseObject("bookmarks");
				parseObject.put("news_id", news.getNews_object_id());
				parseObject.put("user_id",ParseUser.getCurrentUser().getObjectId());
				parseObject.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(ParseException e) {
						
						if(e==null)
						{
							bookmarks bms=new bookmarks();
							bms.setNews_id(news.getNews_object_id());
							bms.setUser_id(ParseUser.getCurrentUser().getObjectId());
							bms.setBookmark_id(parseObject.getObjectId());
							bmdDao.insert(bms);
						}
					}
				});
				
					
				}
				
			}
		});
		if(IsBookmarked(news.getNews_object_id()))
			iv_Bookmarked.setImageResource(R.drawable.ic_bookmarked);
		else
			iv_Bookmarked.setImageResource(R.drawable.ic_not_bookmarked);
		
		
	}
	private void initDaos()
	{
		OpenHelper helper = new DaoMaster.DevOpenHelper(this, "flt-db", null);
		db = helper.getWritableDatabase();
		daoMaster = new DaoMaster(db);
		daoSession = daoMaster.newSession();
		bmdDao=daoSession.getBookmarksDao();
	}
	private boolean IsBookmarked(String newsId)
	{
		bmdList=bmdDao.queryBuilder().list();
		
		for(int i=0;i<bmdList.size();i++)
			if(bmdList.get(i).getNews_id().equals(newsId))
				return true;
		return false;
		
	}
	private bookmarks getBookMarkedObject(String newsId)
	{
		bmdList=bmdDao.queryBuilder().list();
		
		for(int i=0;i<bmdList.size();i++)
			if(bmdList.get(i).getNews_id().equals(newsId))
				return bmdList.get(i);
		return null;
		
	}
	
	private void initialisePaging() {

		

		for (int i=0;i<imageList.size();i++) {
			ImageFragment imageFragment = ImageFragment.newInstance(imageList.get(i).getImage_url(),true);
			productImageFragment.add(imageFragment);

		}

		mPagerAdapter = new ImagePagerAdapter(getSupportFragmentManager(), productImageFragment);

		mPager.setAdapter(mPagerAdapter);
		mPager.setOffscreenPageLimit(1);
		indicator.setViewPager(mPager);
	}
	private void getImages()
	{
		pdDialog.show();
		pdDialog.setCancelable(false);
		News news=Constants.selectedNews;
		
		ParseQuery<ParseObject> parseQuery=ParseQuery.getQuery("images");
		parseQuery.whereEqualTo("add_id", news.getNews_object_id());
		parseQuery.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if(pdDialog.isShowing())
					pdDialog.dismiss();
				if(e==null)
				{
					if(list.size()>0)
					{
						for(int i=0;i<list.size();i++)
						{
							NewsImage newsImage=new NewsImage();
							newsImage.setObjectId(list.get(i).getObjectId());
							newsImage.setImage_url(list.get(i).getParseFile("image_file").getUrl());
							imageList.add(newsImage);
						}
						Constants.selectedNews.setImageList(imageList);
					}
					initialisePaging();
				}
				
			}
		});
		
	}
	private void showCommentDialog(final News news) {
		final Dialog dialog = new Dialog(NewsDetailsActivity.this);

		// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setTitle("Comments");
		dialog.setContentView(R.layout.dialog_comments);
		ListView lv = (ListView) dialog.findViewById(R.id.lv_comments);
		final 	ProgressBar pd=(ProgressBar)dialog.findViewById(R.id.pbar);
		pd.setVisibility(View.VISIBLE);
		commentListAdapter = new CommentListAdapter(NewsDetailsActivity.this, R.layout.row_comment, cmntList);
		lv.setAdapter(commentListAdapter);

		ParseQuery<ParseObject> query = ParseQuery.getQuery("comments");
		query.whereEqualTo("news_id",news.getNews_object_id());
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				pd.setVisibility(View.INVISIBLE);
				if (e == null) {
					cmntList.clear();
					if (list.size() > 0) {
						int size = list.size();
						for (int i = 0; i < size; i++) {
							cmntList.add(parseCommentObjcet(list.get(i)));

						}
						commentListAdapter.notifyDataSetChanged();
					}
				}

			}
		});
		final EditText edtMessage = (EditText) dialog.findViewById(R.id.et_msg);
		TextView tvSend = (TextView) dialog.findViewById(R.id.tv_send);
		tvSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String msg = edtMessage.getText().toString();
				if (!msg.equals(""))
					postMessage( news.getNews_object_id(),edtMessage,pd);

			}
		});

		// Window window = dialog.getWindow();
		// WindowManager.LayoutParams wlp = window.getAttributes();

		// wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		// window.setAttributes(wlp);
		dialog.show();

	}
	private Comment parseCommentObjcet(ParseObject object) {
		Comment comment = new Comment();
		comment.setUser_name(object.getString("username"));
		comment.setMessage(object.getString("message"));
		String date = object.getCreatedAt().toString();
		String mydate = date.substring(4, 10);
		int size = date.length();
		mydate = mydate + ", " + date.substring(size - 4, size);
		comment.setDate(mydate);
		// news.setDateFound(""+object.get)
		return comment;

	}

	private void postMessage(String news_id,final EditText edtMsg,final ProgressBar pbar) {
		pbar.setVisibility(View.VISIBLE);
		ParseUser user = ParseUser.getCurrentUser();
		ParseObject parseObject = new ParseObject("comments");
		parseObject.put("user_id", user.getObjectId());
		parseObject.put("username", user.getString("profile_name"));
		parseObject.put("news_id", news_id);
		parseObject.put("message", edtMsg.getText().toString());

		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				pbar.setVisibility(View.INVISIBLE);
				if (e == null) {
					
					Comment comnt=new Comment();
					comnt.setUser_name(ParseUser.getCurrentUser().getString("profile_name"));
					comnt.setMessage(edtMsg.getText().toString());
					edtMsg.setText("");
					cmntList.add(comnt);
					commentListAdapter.notifyDataSetChanged();
					
					
				}
				else
					Constants.alert(NewsDetailsActivity.this, "An error occured.");
					

			}
		});
	}
	@Override
	protected void onDestroy() {
		daoMaster.getDatabase().close();
		unregisterReceiver(broadcastReceiver);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.prev_slide_in, R.anim.prev_slide_out);
		// super.onBackPressed();
	}


}
