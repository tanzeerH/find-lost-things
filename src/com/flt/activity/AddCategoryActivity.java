package com.flt.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.xml.sax.DTDHandler;

import com.findlostthings.R;
import com.flt.adapter.SimpleListAdapter;
import com.flt.model.UserPicture;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.ImageColumns;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class AddCategoryActivity extends Activity {

	private EditText edtName;
	private ImageView imgCategory;

	private static final int SELECT_PICTURE = 1;
	private static final int CAMERA_REQUEST = 2;
	private String selectedImagePath;
	private Uri outputFileUri;
	private UserPicture userPic;
	private Bitmap picture;
	private ProgressDialog pdDialog;
	private boolean is_new_image_added = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_category);
		edtName = (EditText) findViewById(R.id.et_cat_name);
		imgCategory = (ImageView) findViewById(R.id.iv_cat_pic);
		pdDialog=new ProgressDialog(AddCategoryActivity.this);
		pdDialog.setMessage("Please wait....");
		imgCategory.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				showOptionsDialog();
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.post_menu, menu);
		return true;
	}

	private void postCategory() {
		String str = edtName.getText().toString();
		if (str.equals(""))
			Constants.alert(AddCategoryActivity.this, "Please insert category name.");
		else if (!is_new_image_added) {
			Constants.alert(AddCategoryActivity.this, "Please insert category image.");
		} else if (picture == null) {
			Constants.alert(AddCategoryActivity.this, "An error occured.");
		}
		else if (!Utility.hasInternet(AddCategoryActivity.this)) {
			Constants.alert(AddCategoryActivity.this, "Please check your internet connection.");
		}else {
			pdDialog.show();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			picture.compress(Bitmap.CompressFormat.PNG, 100, stream);
			saveCategoryImageServer(stream.toByteArray());
		}

	}

	void prepareCamera() {
		final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
				+ "/picFolder/";
		File newdir = new File(dir);
		newdir.mkdirs();
		String file = dir + System.currentTimeMillis() + ".jpg";
		File newfile = new File(file);
		try {
			newfile.createNewFile();
		} catch (IOException e) {
		}

		outputFileUri = Uri.fromFile(newfile);

		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

		startActivityForResult(cameraIntent, CAMERA_REQUEST);
	}

	private void showOptionsDialog() {
		final Dialog dialog = new Dialog(AddCategoryActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_attatchment);
		ArrayList<String> list = new ArrayList<String>();
		list.add("Take Photo");
		list.add("Choose from existing photos");
		list.add("Cancel");

		ListView lv = (ListView) dialog.findViewById(R.id.lv_attatch);
		SimpleListAdapter adapter = new SimpleListAdapter(AddCategoryActivity.this, R.layout.row_simple_list, list);

		lv.setAdapter(adapter);
		Window window = dialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();

		// wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);
		dialog.show();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				if (position == 0) {
					prepareCamera();
				} else if (position == 1) {
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
				}
				dialog.dismiss();

			}
		});

	}

	private void saveCategoryImageServer(byte[] bytes) {

		String filename = ParseUser.getCurrentUser().getObjectId() + System.currentTimeMillis() + "-picture.jpg";
		final ParseFile parseFile = new ParseFile(filename, bytes);
		parseFile.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Log.e("PIC URL", parseFile.getUrl());
					postCategoryInserver(parseFile.getUrl());

				} else {
					if(pdDialog.isShowing())
						pdDialog.dismiss();
					Log.e("PIC URL Error", "error");
				}

			}
		});
	}

	private void postCategoryInserver(String imageUrl) {
		ParseObject parseObject=new ParseObject("category");
		parseObject.put("category_name",edtName.getText().toString());
		parseObject.put("image_url",imageUrl);
		parseObject.saveInBackground(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				if(pdDialog.isShowing())
					pdDialog.dismiss();
				if(e==null)
				{
					Constants.alert(AddCategoryActivity.this,"Category saved.");
				}
				else
				{
					Constants.alert(AddCategoryActivity.this,"An error occured.");
				}
				
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				Uri selectedImageUri = data.getData();
				userPic = new UserPicture(selectedImageUri, getContentResolver());
				try {
					picture = userPic.getBitmap();
					if (picture != null)
						selectedImagePath = userPic.getPath();
				} catch (IOException e) {

					e.printStackTrace();
				}

				if (picture != null) {

					is_new_image_added = true;
					imgCategory.setImageBitmap(picture);

				}
				// System.out.println(selectedImagePath);
				// profilePhotoUrl = selectedImagePath;

			}
		}
		if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
			Log.e("msg", "picture saved");

			userPic = new UserPicture(outputFileUri, getContentResolver());
			try {
				picture = userPic.getBitmap();
				if (picture != null)
					selectedImagePath = userPic.getPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (picture != null) {
				is_new_image_added = true;
				imgCategory.setImageBitmap(picture);
			} else {
				Log.e("ms", "null");
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_post) {
			// save the category
			postCategory();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
