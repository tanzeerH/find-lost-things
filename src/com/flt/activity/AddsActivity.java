package com.flt.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.findlostthings.R;
import com.flt.adapter.GridNewsListAdapter;
import com.flt.adapter.NewsListAdapter;
import com.flt.model.News;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts.Data;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;

@SuppressLint("NewApi")
public class AddsActivity extends BaseActivity {
	private ArrayList<News> newsList = new ArrayList<News>();
	private GridNewsListAdapter newsListAdapter;
	private PullToRefreshGridView pullGvNews;
	private ProgressDialog pdDialog;
	private GridView gvNewsList;
	private boolean refreshFlag = false;
	private ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_adds);
		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		pullGvNews = (PullToRefreshGridView) findViewById(R.id.pull_refresh_grid);
		pullGvNews.setMode(Mode.BOTH);
		pullGvNews.setShowIndicator(false);
		pullGvNews.setPullLabel("Loading...");
		gvNewsList = this.pullGvNews.getRefreshableView();
		gvNewsList.setColumnWidth(getWidth()/2);
		pullGvNews.setOnRefreshListener(new OnRefreshListener<GridView>() {

			@Override
			public void onRefresh(PullToRefreshBase<GridView> refreshView) {
				refreshFlag = true;
				getNews();

				
			}
		});
		gvNewsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Constants.selectedNews = newsList.get(position);
				Intent i = new Intent(AddsActivity.this, NewsDetailsActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

			}
		});
		// createNotification();
		pdDialog = new ProgressDialog(AddsActivity.this);
		pdDialog.setMessage("Please wait....");
		if (!Utility.hasInternet(AddsActivity.this))
			Constants.alert(AddsActivity.this, "Please check your internet connection.");
		else
			getNews();
	}

	private void getNews() {
		if (!refreshFlag)
			pdDialog.show();
		ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("news");
		parseQuery.whereEqualTo("category_id", Utility.selectedCategory.getObjectId());
		if (refreshFlag && newsList.size() > 0) {
			int size = newsList.size();
			Date createdDate = newsList.get(size - 1).getModified_at();
			Log.e("date",createdDate.toString());
			parseQuery.whereGreaterThan("createdAt", createdDate);
		}
		parseQuery.setLimit(10);
		parseQuery.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if (pdDialog.isShowing())
					pdDialog.dismiss();
				if (refreshFlag) {
					pullGvNews.onRefreshComplete();
					refreshFlag = false;
					Log.e("status", "refresh complete");
				}
				if (e == null) {
					if (list.size() > 0) {
						int size = list.size();
						for (int i = 0; i <size; i++) {
							newsList.add(getNewsObjcet(list.get(i)));
						}
					}
					setAdapter();

				} else {
					e.printStackTrace();
				}

			}
		});
	}
	private int getImageWidth()
	{
		DisplayMetrics displayMetrics=getResources().getDisplayMetrics();
		int width=displayMetrics.widthPixels-(int) displayMetrics.density*46;
		Log.e("image width",""+width/2);
		return width/2;
	}
	private int  getWidth()
	{
		DisplayMetrics displayMetrics=getResources().getDisplayMetrics();
		int width=displayMetrics.widthPixels;
		Log.e("width",""+width/2);
		return width;
	}
	private void setAdapter() {
		Log.e("size", "" + newsList.size());
		newsListAdapter = new GridNewsListAdapter(AddsActivity.this, R.layout.row_news,getImageWidth(), newsList);
		gvNewsList.setAdapter(newsListAdapter);

	}

	private News getNewsObjcet(ParseObject object) {
		News news = new News();
		news.setNews_object_id(object.getObjectId());
		news.setProfile_name(object.getString("profile_name"));
		news.setTitle(object.getString("title"));
		news.setDescription(object.getString("description"));
		news.setUniversity_id(object.getString("university_id"));
		news.setUniversity_name(object.getString("university"));
		String price = object.getString("price");
		news.setPrice(Double.valueOf(price));
		String date = object.getCreatedAt().toString();
		String mydate = date.substring(4, 10);
		int size = date.length();
		mydate = mydate + ", " + date.substring(size - 4, size);
		news.setCreated_at(mydate);
		news.setUserObjectId(object.getString("user_id"));
		news.setModified_at(object.getCreatedAt());
		news.setImageUrl(object.getString("image_url"));
		// news.setDateFound(""+object.get)
		return news;

	}

	/*
	 * private String getMonth(int n) { if (n == 0) return "Jan"; else if (n ==
	 * 1) return "Feb"; else if (n == 2) return "Mar"; else if (n == 3) return
	 * "Apr"; else if (n == 4) return "May"; else if (n == 5) return "Jun"; else
	 * if (n == 6) return "Jul"; else if (n == 7) return "Aug"; else if (n == 8)
	 * return "Sep"; else if (n == 8) return "Oct"; else if (n == 10) return
	 * "Nov"; else return "Dec";
	 * 
	 * }
	 */
	private void createNotification() {
		Intent i = new Intent(AddsActivity.this, PostAddActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(AddsActivity.this, 0, i, 0);
		Notification notification = new Notification.Builder(AddsActivity.this).setContentTitle("New message from me.")
				.setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher).setContentText("Message")
				.setContentIntent(pIntent).build();

		NotificationManager notificationManager = (NotificationManager) getSystemService("notification");

		notificationManager.notify(0, notification);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.news_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_add) {
			Intent i = new Intent(AddsActivity.this, PostNewsActivity.class);
			startActivity(i);
			return true;
		} else if (id == android.R.id.home) {
			finish();
			overridePendingTransition(R.anim.prev_slide_in, R.anim.prev_slide_out);
		}
		return super.onOptionsItemSelected(item);
	}
}
