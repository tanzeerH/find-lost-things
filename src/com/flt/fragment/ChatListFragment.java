package com.flt.fragment;

import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.activity.LoginActivity;
import com.flt.activity.MessageActivity;
import com.flt.adapter.ChatListAdapter;
import com.flt.model.ChatThread;
import com.flt.model.News;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ChatListFragment extends Fragment{
	private ArrayList<ChatThread> chatThreads=new ArrayList<ChatThread>();
	private ChatListAdapter  chatListAdapter;
	private ListView lvChatList;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_chat_history,null,false);
		lvChatList=(ListView)v.findViewById(R.id.lv_chatlist);
		return v;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		chatListAdapter=new ChatListAdapter(getActivity(),R.layout.row_simple_list,chatThreads);
		lvChatList.setAdapter(chatListAdapter);
		lvChatList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				ParseUser user=ParseUser.getCurrentUser();
				News news=new News();
				ChatThread cThread=chatThreads.get(position);
				if(cThread.getUser1_ID().equals(user.getObjectId()))
				{
					news.setUserObjectId(cThread.getUser2_id());
					news.setProfile_name(cThread.getUser2_name());
					
				}
				else
				{
					news.setUserObjectId(cThread.getUser1_ID());
					news.setProfile_name(cThread.getUser1_name());
				}
				Constants.selectedNews=news;
				
				Intent intent=new Intent(getActivity(),MessageActivity.class);
				intent.putExtra("chat_id",chatThreads.get(position).getChatThreadID());
				intent.putExtra("news_poster_name",Constants.selectedNews.getProfile_name());
				intent.putExtra("news_poster_id",Constants.selectedNews.getUserObjectId());
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				
			}
		});
		getChatThreads("user1");
	}
	private void getChatThreads(final String colName)
	{
		if(Utility.hasInternet(getActivity()))
		{
		ParseUser user=ParseUser.getCurrentUser();
		ParseQuery<ParseObject> query1=ParseQuery.getQuery("chats");
		query1.whereEqualTo(colName,user.getObjectId());
		
		query1.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> list, ParseException e) {
				
				if(e==null)
				{
					int size=list.size();
					Log.e("chat thread size",size+"");
					if(size>0)
					{
						for(int i=0;i<size;i++)
						{
							chatThreads.add(getchatThreadObjcet(list.get(i)));
						}
						chatListAdapter.notifyDataSetChanged();
					}
					if(colName.equals("user1"))
						getChatThreads("user2");
				}
				
				
			}
		});
		}
		else
			Constants.alert(getActivity(),"Please check your internet connection.");
	}
	private ChatThread getchatThreadObjcet(ParseObject object)
	{
		ChatThread chatThread=new ChatThread();
		chatThread.setChatThreadID(object.getObjectId());
		chatThread.setUser1_ID(object.getString("user1"));
		chatThread.setUser2_id(object.getString("user2"));
		chatThread.setUser1_name(object.getString("user1_name"));
		chatThread.setUser2_name(object.getString("user2_name"));
		
		return chatThread;
		
	}
	

}
