package com.flt.fragment;

import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.activity.AddsActivity;
import com.flt.adapter.NewsListAdapter;
import com.flt.model.News;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class NewsFargment extends Fragment{
	private ArrayList<News> newsList=new ArrayList<News>();
	private NewsListAdapter newsListAdapter;
	private PullToRefreshListView lvNews;
	private ProgressDialog pdDialog;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View v=inflater.inflate(R.layout.post_news_activity,null,false);
		lvNews=(PullToRefreshListView)v.findViewById(R.id.lv_news);
		pdDialog=new ProgressDialog(getActivity());
		pdDialog.setMessage("Please wait....");
		
		lvNews.setMode(Mode.BOTH);
		lvNews.setShowIndicator(true);
		lvNews.setPullLabel("Loading...");
		lvNews.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh(PullToRefreshBase refreshView) {
				//refresh clicked
				Log.e("refresh", "refreshed");
				
			}
		});
		
		
		return v;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getNews();
		
	}
	private void getNews()
	{
		pdDialog.show();
		ParseQuery<ParseObject> parseQuery=ParseQuery.getQuery("news");
		parseQuery.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> list, ParseException e) {
				pdDialog.dismiss();
				if(e==null)
				{
					if(list.size()>0)
					{
						int size=list.size();
						for(int i=0;i<size;i++)
						{
						 newsList.add(getNewsObjcet(list.get(i)));
						}
					}
					setAdapter();
					
				}
				else
				{
					e.printStackTrace();
				}
				
			}
		});
	}
	private void setAdapter()
	{
		newsListAdapter=new NewsListAdapter(getActivity(),R.layout.row_news,newsList);
		lvNews.setAdapter(newsListAdapter);
		
	}
	private News getNewsObjcet(ParseObject object)
	{
		News news=new News();
		news.setNews_object_id(object.getObjectId());
		news.setProfile_name(object.getString("profile_name"));
		news.setTitle(object.getString("title"));
		news.setUserObjectId(object.getString("user_id"));
		//news.setDateFound(""+object.get)
		return news;
		
	}
}
