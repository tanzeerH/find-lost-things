package com.flt.fragment;

import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.activity.AddsActivity;
import com.flt.adapter.CategoryListAdapter;
import com.flt.model.Category;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class CategoryFragment extends Fragment{
	public static ArrayList<Category> catList=new ArrayList<Category>();
	private CategoryListAdapter categoryListAdapter;
	private ListView lvCategory;
	private ProgressDialog pdDialog;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_category,null,false);
		lvCategory=(ListView)v.findViewById(R.id.lv_news);
		pdDialog=new ProgressDialog(getActivity());
		pdDialog.setMessage("Please wait....");
		pdDialog.setCancelable(false);
		return v;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if(!Utility.hasInternet(getActivity()))
			Constants.alert(getActivity(),"Please check your internet connection.");
		else 
			getCategories();
		
	}
	private void getCategories()
	{
		pdDialog.show();
		ParseQuery<ParseObject> parseQuery=ParseQuery.getQuery("category");
		parseQuery.findInBackground(new FindCallback<ParseObject>() {
			
			@Override
			public void done(List<ParseObject> list, ParseException e) {
				pdDialog.dismiss();
				catList.clear();
				if(e==null)
				{
					if(list.size()>0)
					{
						int size=list.size();
						for(int i=0;i<size;i++)
						{
						 catList.add(getCategoryObjcet(list.get(i)));
						}
					}
					setAdapter();
					
				}
				else
				{
					e.printStackTrace();
				}
				
			}
		});
	}
	private void setAdapter()
	{
		categoryListAdapter=new CategoryListAdapter(getActivity(), R.layout.row_category,catList);
		lvCategory.setAdapter(categoryListAdapter);
		lvCategory.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				Utility.selectedCategory=catList.get(position);
				Intent intent=new Intent(getActivity(),AddsActivity.class);
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				
			}
		});
		
	}
	private Category getCategoryObjcet(ParseObject object)
	{
		Category category=new Category();
		
		category.setName(object.getString("category_name"));
		category.setObjectId(object.getObjectId());
		category.setImageUrl(object.getString("image_url"));
		//news.setDateFound(""+object.get)
		return category;
		
	}

}
