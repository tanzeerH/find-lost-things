package com.flt.fragment;


import com.findlostthings.R;
import com.flt.lazylist.FileCache;
import com.flt.lazylist.ImageLoader;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;


public class ImageFragment extends Fragment {

	private static final String TAG = ImageFragment.class.getSimpleName();
	// private ProgressDialog pDialog;

	private GestureDetector gestureDetector;

	ImageView ProductImage;
	ImageView EnlargeImage;
	ProgressBar UploadProgress;


	String imageUrl;
	private ImageLoader imageLoader;

	Activity activity;

	// ImageLoader imageLoader;

	public ImageFragment() {
		// TODO Auto-generated constructor stub
	}

	public static ImageFragment newInstance(String imageUrl, Boolean enlargeFlag) {
		ImageFragment f = new ImageFragment();
		Log.d(TAG, "image url in new instance = " + imageUrl);
		Bundle args = new Bundle();
		args.putBoolean("enlarge_flag", enlargeFlag);
		args.putString("product_detail_image", imageUrl);
		f.setArguments(args);
		return (f);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setUserVisibleHint(true);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		imageUrl = getArguments().getString("product_detail_image");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_image, container, false);
		ProductImage = (ImageView) view.findViewById(R.id.ivProductImage);
		UploadProgress = (ProgressBar) view.findViewById(R.id.pbProgress);

		EnlargeImage = (ImageView) view.findViewById(R.id.ivEnlarge);
		EnlargeImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Toast.makeText(activity, "enlarge button is pressed.",
				// Toast.LENGTH_SHORT).show();
				/*Intent intent = new Intent(getActivity(), TouchImageViewActivity.class);
				ViewPager vp = (ViewPager) getActivity().findViewById(R.id.pager);
				ProductDetailsActivity.pageNumber = vp.getCurrentItem();
				Log.d(">>>><<<", "current page = " + ProductDetailsActivity.pageNumber);
				// intent.putExtra("ImageAddress", imageUrl);
				startActivity(intent);
				activity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);*/

			}
		});

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		activity = getActivity();
		imageLoader=new ImageLoader(activity);
		imageLoader.DisplayImage(imageUrl,ProductImage);
		
	}

	

}
