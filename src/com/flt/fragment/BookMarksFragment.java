package com.flt.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.findlostthings.R;
import com.flt.activity.AddsActivity;
import com.flt.activity.MyNewsDetailsActivity;
import com.flt.activity.NewsDetailsActivity;
import com.flt.activity.PostAddActivity;
import com.flt.adapter.NewsListAdapter;
import com.flt.dao.DaoMaster;
import com.flt.dao.DaoSession;
import com.flt.dao.bookmarks;
import com.flt.dao.bookmarksDao;
import com.flt.dao.DaoMaster.OpenHelper;
import com.flt.model.News;
import com.flt.utility.Constants;
import com.flt.utility.Utility;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Fragment;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class BookMarksFragment extends Fragment{
	private ArrayList<News> newsList = new ArrayList<News>();
	private NewsListAdapter newsListAdapter;
	private PullToRefreshListView lvNews;
	private ProgressDialog pdDialog;
	private ListView lvNewsList;
	
	private DaoMaster daoMaster;
	private DaoSession daoSession;
	private bookmarksDao bmdDao;
	
	private SQLiteDatabase db;
	private List<bookmarks> bmdList;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.post_news_activity,null,false);
		lvNews = (PullToRefreshListView) v.findViewById(R.id.lv_news);
		lvNews.setMode(Mode.BOTH);
		lvNews.setShowIndicator(false);
		lvNews.setPullLabel("Loading...");
		lvNewsList = this.lvNews.getRefreshableView();
		lvNews.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {

			}
		});
		lvNewsList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Constants.selectedNews = newsList.get(position-1);
				Intent i = new Intent(getActivity(),NewsDetailsActivity.class);
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

			}
		});
		//createNotification();
		pdDialog = new ProgressDialog(getActivity());
		pdDialog.setMessage("Please wait....");
		return v;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initDaos();
		setAdapter();
		if (!Utility.hasInternet(getActivity()))
			Constants.alert(getActivity(), "Please check your internet connection.");
		else
			getNews();
	}
	private void initDaos()
	{
		OpenHelper helper = new DaoMaster.DevOpenHelper(getActivity(), "flt-db", null);
		db = helper.getWritableDatabase();
		daoMaster = new DaoMaster(db);
		daoSession = daoMaster.newSession();
		bmdDao=daoSession.getBookmarksDao();
	}
	private void getNews() {
		pdDialog.show();
		bmdList=bmdDao.queryBuilder().list();
		Log.e("bms size",""+bmdList.size());
		ArrayList<String> bms=new ArrayList<String>();
		for(int i=0;i<bmdList.size();i++)
		{
			bms.add(bmdList.get(i).getNews_id());
		}
		ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("news");
		parseQuery.whereContainedIn("objectId",bms);
		parseQuery.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				pdDialog.dismiss();
				Log.e("size of book marks",""+list.size());
				if (e == null) {
					if (list.size() > 0) {
						int size = list.size();
						for (int i = 0; i < size; i++) {
							newsList.add(getNewsObjcet(list.get(i)));
						}
					}
					newsListAdapter.notifyDataSetChanged();

				} else {
					e.printStackTrace();
				}

			}
		});
	}

	private void setAdapter() {
		Log.e("size", "" + newsList.size());
		newsListAdapter = new NewsListAdapter(getActivity(), R.layout.row_news, newsList);
		lvNewsList.setAdapter(newsListAdapter);

	}

	private News getNewsObjcet(ParseObject object) {
		News news = new News();
		news.setNews_object_id(object.getObjectId());
		news.setProfile_name(object.getString("profile_name"));
		news.setTitle(object.getString("title"));
		news.setDescription(object.getString("description"));
		news.setUniversity_id(object.getString("university_id"));
		news.setUniversity_name(object.getString("university"));
		String price = object.getString("price");
		news.setPrice(Double.valueOf(price));
		String date=object.getCreatedAt().toString();
		String mydate=date.substring(4,10);
		int size=date.length();
		mydate=mydate+", "+date.substring(size-4,size);
		news.setCreated_at(mydate);
		news.setUserObjectId(object.getString("user_id"));
		// news.setDateFound(""+object.get)
		return news;

	}
	@Override
	public void onDestroyView() {
		daoMaster.getDatabase().close();
		super.onDestroyView();
	}
	
}
