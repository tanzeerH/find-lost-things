package com.flt.fragment;

import com.findlostthings.R;
import com.flt.activity.AddCategoryActivity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class AdminFragment extends Fragment{
	
	private Button btnAddCategory,btnEditCategory;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_admin,null,false);
		btnAddCategory=(Button)v.findViewById(R.id.btn_add_cat);
		btnEditCategory=(Button)v.findViewById(R.id.btn_edit_cat);
		btnAddCategory.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent i=new Intent(getActivity(),AddCategoryActivity.class);
				startActivity(i);
			}
		});
		
		btnEditCategory.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
			}
		});
		return v;
	}

}
