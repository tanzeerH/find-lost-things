package com.flt.adapter;

import java.util.List;

import com.findlostthings.R;
import com.flt.model.NavMenuItem;



import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class NavDrawerAdapter extends ArrayAdapter<NavMenuItem>{

	private Context mContext;
	private SQLiteDatabase db;
	
	public NavDrawerAdapter(Context context, int resource, List<NavMenuItem> objects) {
		super(context, resource, objects);
		mContext=context;
		
	}
	private class ViewHolder{
		TextView tvName;
		ImageView ivIcon;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder=null;
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    		if (convertView == null) {
    				 convertView = mInflater.inflate(R.layout.row_nav_menu, null);
    			
    			holder = new ViewHolder();
    			holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
    			holder.ivIcon = (ImageView) convertView.findViewById(R.id.iv_icon);

    			convertView.setTag(holder);
    		} else {
    			holder = (ViewHolder) convertView.getTag();
    		}
    	holder.tvName.setText(getItem(position).getName());
    	holder.ivIcon.setImageResource(getItem(position).getId());
    	
		
		return convertView;
	}
	

}
