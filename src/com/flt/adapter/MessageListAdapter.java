package com.flt.adapter;

import java.util.ArrayList;
import java.util.List;


import com.findlostthings.R;
import com.flt.activity.AddsActivity;
import com.flt.activity.PostNewsActivity;
import com.flt.model.Comment;
import com.flt.model.Message;
import com.flt.model.News;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MessageListAdapter extends ArrayAdapter<Message> {

	private Context mContext;
	private String userId;
	private String userName;

	public MessageListAdapter(Context context, int resource, ArrayList<Message> objects) {
		super(context, resource, objects);
		this.mContext = context;
		userId = ParseUser.getCurrentUser().getObjectId();
		userName = ParseUser.getCurrentUser().getUsername();
	}

	private class ViewHolderSend {
		ImageView ivPic;
		TextView tvMessage;
	}

	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderSend holderSend = null;
		Message message = getItem(position);
		if (message.getSenderId().equals(userId)) {
			
			LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			
			if (convertView == null || ((ViewHolderSend) convertView.getTag(R.integer.type_1)) == null) {
				convertView = mInflater.inflate(R.layout.row_message_send, null);

				holderSend = new ViewHolderSend();

				holderSend.tvMessage = (TextView) convertView.findViewById(R.id.tv_message);
				convertView.setTag(R.integer.type_1, holderSend);

			} else {
				holderSend = (ViewHolderSend) convertView.getTag(R.integer.type_1);
			}
			holderSend.tvMessage.setText(""+message.getMessage());
		} else
		{
			
			LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			if (convertView == null || ((ViewHolderSend) convertView.getTag(R.integer.type_2)) == null) {
				convertView = mInflater.inflate(R.layout.row_message_received, null);

				holderSend = new ViewHolderSend();

				holderSend.tvMessage = (TextView) convertView.findViewById(R.id.tv_message);
				convertView.setTag(R.integer.type_2, holderSend);

			} else {
				holderSend = (ViewHolderSend) convertView.getTag(R.integer.type_2);
			}
			holderSend.tvMessage.setText(message.getMessage());
		}

		return convertView;
	}

}
