package com.flt.adapter;

import java.util.ArrayList;
import java.util.List;

import com.flt.fragment.ImageFragment;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ImagePagerAdapter extends FragmentStatePagerAdapter {

	private List<ImageFragment> fragments;

	// protected OnItemChangeListener mOnItemChangeListener;
	// protected int mCurrentPosition = -1;

	public ImagePagerAdapter(FragmentManager fm, ArrayList<ImageFragment> fragments) {
		super(fm);
		this.fragments = fragments;
	}

	// @Override
	// public void setPrimaryItem(View container, int position, Object object) {
	// Log.d("<<<<>>>", "position = " + position);
	// if (mCurrentPosition == position) return;
	// mCurrentPosition = position;
	// if (mOnItemChangeListener != null)
	// mOnItemChangeListener.onItemChange(mCurrentPosition);
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
	 */
	@Override
	public Fragment getItem(int position) {
		return this.fragments.get(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.view.PagerAdapter#getCount()
	 */
	@Override
	public int getCount() {
		return this.fragments.size();
	}

	// public int getCurrentPosition() { return mCurrentPosition; }
	// public void setOnItemChangeListener(OnItemChangeListener listener) {
	// mOnItemChangeListener = listener;
	// }

}
