package com.flt.adapter;

import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.lazylist.ImageLoader;
import com.flt.model.PostImage;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageListAdapter extends ArrayAdapter<PostImage> {

	private Context mContext;
	ImageLoader imageLoader;
	public ImageListAdapter(Context context, int resource, ArrayList<PostImage> objects) {
		super(context, resource, objects);
		this.mContext=context;
		imageLoader=new ImageLoader((Activity)context);
	}
	private class ViewHolder{
		TextView ivImage;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.e("position", "" + position);
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.grid, null);
			holder = new ViewHolder();
			holder.ivImage = (TextView) convertView.findViewById(R.id.ivPhoto);
			
			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();

		
		holder.ivImage.setText(""+getItem(position).getPos());

		return convertView;
	}

}
