package com.flt.adapter;

import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.activity.MessageActivity;
import com.flt.activity.AddsActivity;
import com.flt.activity.PostNewsActivity;
import com.flt.lazylist.ImageLoader;
import com.flt.model.Comment;
import com.flt.model.News;
import com.flt.utility.Constants;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.sax.StartElementListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class GridNewsListAdapter extends ArrayAdapter<News> {

	private Context mContext;
	private ArrayList<Comment> cmntList = new ArrayList<Comment>();
	CommentListAdapter commentListAdapter;
	private int imageWidth;
	private ImageLoader imageLoader;

	public GridNewsListAdapter(Context context, int resource,int imageWidth, ArrayList<News> objects) {
		super(context, resource, objects);
		this.mContext = context;
		this.imageWidth=imageWidth;
		imageLoader=new ImageLoader((Activity)context);
	}

	private class ViewHolder {
		TextView tvTitle;
		TextView tvPrice;
		ImageView ivImage;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.row_news_grid, null);

			holder = new ViewHolder();
			holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
			holder.tvPrice = (TextView) convertView.findViewById(R.id.tv_price);
			holder.ivImage=(ImageView)convertView.findViewById(R.id.iv_image);
			LayoutParams layoutParams=(LayoutParams)holder.ivImage.getLayoutParams();
			layoutParams.width=imageWidth;
			layoutParams.height=imageWidth;
			holder.ivImage.setLayoutParams(layoutParams);
			
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final News news = getItem(position);
		holder.tvTitle.setText(news.getTitle());
		Log.e("title",news.getTitle());
		// holder.tvDate.setText(news.getDateFound());
		
		holder.tvPrice.setText(news.getPrice()+" TK");
		if(news.getImageUrl()==null || news.getImageUrl().equals(""))
			holder.ivImage.setImageResource(R.drawable.placeholder);
		else
			imageLoader.DisplayImage(news.getImageUrl(),holder.ivImage);
		

		return convertView;
	}

	private Comment parseCommentObjcet(ParseObject object) {
		Comment comment = new Comment();
		comment.setUser_name(object.getString("username"));
		comment.setMessage(object.getString("message"));
		// news.setDateFound(""+object.get)
		return comment;

	}

	private void postMessage(String msg, String news_id) {
		ParseUser user = ParseUser.getCurrentUser();
		ParseObject parseObject = new ParseObject("comments");
		parseObject.put("user_id", user.getObjectId());
		parseObject.put("username", user.getUsername());
		parseObject.put("news_id", news_id);
		parseObject.put("message", msg);

		parseObject.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				if (e == null) {
					Constants.alert(mContext, "Comment posted.");
				}

			}
		});
	}

	private void showCommentDialog(final News news) {
		final Dialog dialog = new Dialog(mContext);

		// dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_comments);
		ListView lv = (ListView) dialog.findViewById(R.id.lv_comments);
		commentListAdapter = new CommentListAdapter(mContext, R.layout.row_comment, cmntList);
		lv.setAdapter(commentListAdapter);

		ParseQuery<ParseObject> query = ParseQuery.getQuery("comments");
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if (e == null) {
					if (list.size() > 0) {
						int size = list.size();
						for (int i = 0; i < size; i++) {
							cmntList.add(parseCommentObjcet(list.get(i)));

						}
						commentListAdapter.notifyDataSetChanged();
					}
				}

			}
		});
		final EditText edtMessage = (EditText) dialog.findViewById(R.id.et_msg);
		TextView tvSend = (TextView) dialog.findViewById(R.id.tv_send);
		tvSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String msg = edtMessage.getText().toString();
				if (!msg.equals(""))
					postMessage(edtMessage.getText().toString(), news.getNews_object_id());

			}
		});

		// Window window = dialog.getWindow();
		// WindowManager.LayoutParams wlp = window.getAttributes();

		// wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		// window.setAttributes(wlp);
		dialog.show();

	}

}
