package com.flt.adapter;

import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.model.ChatThread;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChatListAdapter extends ArrayAdapter<ChatThread> {

	private Context mContext;
	public ChatListAdapter(Context context, int resource, ArrayList<ChatThread> objects) {
		super(context, resource, objects);
		this.mContext=context;
	}
	private class ViewHolder{
		TextView tvName;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder=null;
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    		if (convertView == null) {
    			
    				 convertView = mInflater.inflate(R.layout.row_simple_list, null);
    			
    			holder = new ViewHolder();
    			holder.tvName = (TextView) convertView.findViewById(R.id.tvop);
  
    			convertView.setTag(holder);
    		} else {
    			holder = (ViewHolder) convertView.getTag();
    		}
    	ChatThread chatThread=getItem(position);
    	String user1=ParseUser.getCurrentUser().getObjectId();
    	if(chatThread.getUser1_ID().equals(user1))
    	{
    		holder.tvName.setText(""+chatThread.getUser2_name());
    	}
    	else
    		holder.tvName.setText(""+chatThread.getUser1_name());
  
            		
		
		return convertView;
	}

}
