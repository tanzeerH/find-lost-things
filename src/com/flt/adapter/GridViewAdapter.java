package com.flt.adapter;

import java.util.ArrayList;




import android.content.Context;
import android.graphics.Bitmap;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


public class GridViewAdapter extends BaseAdapter {
	private Context mContext;
	ArrayList<Bitmap> imgList;
	int width = 0;

	public GridViewAdapter(Context c) {
		mContext = c;
	}

	public GridViewAdapter(Context c, int width, ArrayList<Bitmap> list) {
		this.mContext = c;
		this.width = width;
		this.imgList = list;
	}

	public int getCount() {
		return imgList.size();

	}

	public Object getItem(int position) {
		return imgList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			imageView = new ImageView(mContext);
			// imageView.setBackgroundResource(R.drawable.border);
			imageView.setLayoutParams(new GridView.LayoutParams(width, width));
			Log.e("width", "" + width);
			Log.e("size", "" + imgList.size());
						imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		} else {
			imageView = (ImageView) convertView;
		}
		imageView.setImageBitmap((Bitmap)getItem(position));
		
		return imageView;
	}

}
