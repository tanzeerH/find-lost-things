package com.flt.adapter;

import java.util.ArrayList;
import java.util.List;

import com.findlostthings.R;
import com.flt.activity.AddsActivity;
import com.flt.activity.PostNewsActivity;
import com.flt.model.Comment;
import com.flt.model.News;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class CommentListAdapter extends ArrayAdapter<Comment> {

	private Context mContext;
	public CommentListAdapter(Context context, int resource, ArrayList<Comment> objects) {
		super(context, resource, objects);
		this.mContext=context;
	}
	private class ViewHolder{
		TextView tvUser;
		TextView tvDate;
		TextView tvMessage;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder=null;
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    		if (convertView == null) {
    			
    				 convertView = mInflater.inflate(R.layout.row_comment, null);
    			
    			holder = new ViewHolder();
    			
    			holder.tvUser = (TextView) convertView.findViewById(R.id.tv_user);
    			holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
    			holder.tvMessage= (TextView) convertView.findViewById(R.id.tv_comment);
    			convertView.setTag(holder);
    		} else {
    			holder = (ViewHolder) convertView.getTag();
    		}
    	Comment comment=getItem(position);
    	
    	holder.tvUser.setText(comment.getUser_name());
    	holder.tvMessage.setText(comment.getMessage());
        holder.tvDate.setText(comment.getDate());  		
		
		return convertView;
	}
	

}
